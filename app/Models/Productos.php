<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Productos extends Model
{
    protected $table='producto';
    
	protected $primaryKey = 'id';
    
    protected $fillable = array(
        'nombre', 'descripcion','precio','estatus','imagen','idcategorias');
    
    protected $hidden = ['created_at','updated_at'];

    public function categoria()
    {
    	return $this->belongsTo('App\Models\Categorias','idcategoria','id');
    }
}
