<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Categorias extends Model
{
    protected $table='categoria';
    
	protected $primaryKey = 'id';
    
    protected $fillable = array(
        'nombre', 'descripcion');

    protected $hidden = ['created_at','updated_at'];

    public function productos()
    {
    	return $this->hasMany('App\Models\Productos','id','idcategoria');
    }
}
