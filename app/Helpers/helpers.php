<?php

//Devuelve la ruta de la foto
    function getImagen($id) {   
        $foto = DB::table('producto')->where('id', $id)->pluck('imagen')->implode(' ');
        if($foto == 'default_imagen.png'){
            return asset('assets/img/productos/default_imagen.png');
        } else {
            return asset('assets/img/productos/'.$foto);
        }
        
    }

  	//Formato de fecha
    function getFormatoFecha($date) {
        
        return date('d-m-Y',strtotime($date));
    }

    //
    function categorias() {
        
        $categoria = DB('categoria')->select(['id','nombre'])
                        ->get();
        
        return $categoria;
    }

    function set_active($path, $active = 'active') {

        return call_user_func_array('Request::is', (array)$path) ? $active : '';

    }

