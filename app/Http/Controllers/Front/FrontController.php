<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Categorias;
use App\Models\Productos;

class FrontController extends Controller
{
    public function getInicio()
    {
    	$productos = Productos::where('estatus', 1)
                    ->paginate(6);

    	return view('front.home',['categorias'=>$this->getCategorias(), 'productos' => $productos]);
    }

    public function getShow($id)
    {
    	$producto = Productos::findOrFail($id);

    	return view('front.show',['categorias'=>$this->getCategorias(),'producto'=>$producto]);
    }

    public function getCategoria(Request $request)
    {
        $nombrecategoria = $request->categoria;
        $categoria = Categorias::where('nombre',$nombrecategoria)
                    ->pluck('id')->implode('');
    
        $productos = Productos::where('idcategoria',$categoria)
                            ->where('estatus',1)
                            ->paginate(9);

        return view('front.categoria',['categorias'=>$this->getCategorias(), 
                                        'categoria'=>$nombrecategoria,
                                        'productos'=>$productos] );
    }

    protected function getCategorias()
    {
        $categorias = Categorias::orderBy('nombre','asc')->get();

        return $categorias;
    }
}
