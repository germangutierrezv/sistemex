<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CategoriaRequest;


use App\Models\Categorias;

class CategoriaController extends Controller
{
    public function __construct()
    {
        $this->middleware('sentry.admin');
    }

    public function index()
    {
    	$categorias = Categorias::orderBy('nombre','asc')
    					->paginate(10);
    	
    	return view('admin.categorias.index',['categorias'=>$categorias]);
    }

    public function create()
    {
    	return view('admin.categorias.create');
    }

    public function show($id)
    {
    	$categoria = Categorias::findOrFail($id);

    	return view('admin.categorias.show',['categoria'=>$categoria]);
    }

    public function store(CategoriaRequest $request)
    {
    	$categoria = new Categorias();
    	$categoria->nombre = $request->nombre;
    	$categoria->descripcion = $request->descripcion;
    	
    	$guardada = $categoria->save();

    	$message = $guardada? 'Categoria Creada!!':'La categoria no puede ser creada';
    	$status = $guardada? 'success':'danger';

    	return redirect('/categorias')
                ->with('message',$message)
                ->with('status',$status);

    }

    public function edit($id)
    {
    	$categoria = Categorias::findOrFail($id);

    	return view('admin.categorias.edit',['categoria'=>$categoria]);
    }

    public function update(CategoriaRequest $request, $id)
    {
        $categoria = Categorias::findOrFail($id);
        
        $input = $request->all();

        $update = $categoria->fill($input)->save();

        $message = $update? 'Categoria Actualizada!!':'La categoria no puede ser actualizada';
    	$status = $update? 'success':'danger';

    	return redirect('/categorias')
                ->with('message',$message)
                ->with('status',$status);

    }

    public function destroy($id)
    {
    	$categoria = Categorias::findOrFail($id);

    	$del = $categoria->delete();

    	$message = $del? 'Categoria eliminada!!':'No se puede eliminar la categoria';
    	$status = $del? 'success':'danger';

    	return redirect('categorias')
				->with('message',$message)
                ->with('status',$status);    			 
    }
}
