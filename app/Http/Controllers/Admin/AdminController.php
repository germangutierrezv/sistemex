<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Sentry;

use App\Models\Productos;
use App\Models\Categorias;


class AdminController extends Controller
{
    public function getDashboard()
    {
    	$productos = Productos::all()->count();
    	$categorias = Categorias::all()->count();
    	$usuarios = Sentry::getUserProvider()->createModel()->all()->count();
    	$productosActivos = Productos::where('estatus',1)
    						->count();
    	$usuariosActivos = Sentry::getUserProvider()->createModel()->where('activated',1)->count();

    	$ultimosproductos = Productos::latest()
    						->take(5)
    						->get();

    	return view('admin.dashboard',['categorias'=>$categorias,'productos'=>$productos,
    									'usuarios'=>$usuarios,'productosActivos'=>$productosActivos,
    									'usuariosActivos'=>$usuariosActivos, 'ultimosproductos'=>$ultimosproductos]);
    }
    
}
