<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\ProductoRequest;
use Yajra\Datatables\Datatables;
use DB;

use App\Models\Productos;
use App\Models\Categorias;

class ProductoController extends Controller
{
    public function __construct()
    {
        $this->middleware('sentry.admin');
    }

    public function index()
    {
    	
    	return view('admin.productos.index');
    }

    public function getProductos()
    {
        /*$productos = DB::table('producto as p')
                    ->join('categoria as c','p.idcategoria','=','c.id')->get();
                   */ 
        $productos=Productos::select(['producto.id','producto.nombre','estatus','precio','categoria.nombre as categoria'])
                ->join('categoria','producto.idcategoria','=','categoria.id')->get();

        return Datatables::of($productos)        
                ->editColumn('estatus', function(Productos $producto) {
                     return $producto->estatus? 'Activo':'Inactivo'; 
                 })
                ->make(true);
    }

    public function show($id)
    {
    	$producto = Productos::findOrFail($id);

    	return view('admin.productos.show',['producto'=>$producto]);
    }

    public function create()
    {
        $categorias = Categorias::orderBy('nombre', 'asc')->pluck('nombre', 'id');
                
        return view('admin.productos.create',['categorias'=>$categorias]);	
    }

    public function store(ProductoRequest $request)
    {
        $producto = new Productos();
        $producto->nombre = $request->nombre;
        $producto->descripcion = $request->descripcion;
        $producto->precio = $request->precio;
        $producto->estatus = $request->estatus;
        $producto->idcategoria = $request->idcategoria;
        
        if($request->imagen){
            
            $filename = $producto->nombre . '.'. $request->imagen->getClientOriginalExtension();

            $request->imagen->move(public_path('assets/img/productos'), $filename);

            $producto->imagen = $filename;
        }


        $guardado = $producto->save();

        $message = $guardado ? 'Producto Creado!!':'Producto no pudo guardarse';
        $status = $guardado ? 'success':'danger';

        return redirect('/productos')
                ->with('message',$message)
                ->with('status',$status);
    }

    public function edit($id)
    {
        $producto = Productos::findOrFail($id);

        $categorias = Categorias::orderBy('id', 'desc')->pluck('nombre', 'id');
                
        return view('admin.productos.edit',['categorias'=>$categorias, 'producto'=>$producto]);  
    }

    public function update(ProductoRequest $request, $id)
    {
        $producto= Productos::findOrFail($id);
        $input = $request->all();
        
        if(is_null($request->imagen)){
            $input['imagen'] = $request->imagenAnt;
        }
        
        if($request->imagen){
            
            $filename = $producto->nombre . '.'. $request->imagen->getClientOriginalExtension();

            $request->imagen->move(public_path('assets/img/productos'), $filename);

            $input['imagen'] = $filename;
        }
        
        $guardado = $producto->fill($input)->save();

        $message = $guardado ? 'Producto Actualizado!!':'Producto no pudo actualizarse';
        $status = $guardado ? 'success':'danger';

        return redirect('/productos')
                ->with('message',$message)
                ->with('status',$status);

    }

    public function destroy($id)
    {
        $proyecto = Productos::findOrFail($id);

        $delete = $proyecto->delete();

        $message = $delete ? 'Producto Eliminado!!':'Producto no pudo eliminarse';
        $status = $delete ? 'success':'danger';

        return redirect('/productos')
                ->with('message',$message)
                ->with('status',$status);
    }

    public function activaProducto(Request $request, $id)
    {
        $producto = Productos::findOrFail($request->id);
        $producto->estatus = !$producto->estatus;
        $activa = $producto->save();

        $message = $activa ? 'Producto Cambio de estatus!!':'Producto no puede cambiar de estatus';
        $status = $activa ? 'success':'danger';

        return redirect('/productos')
                ->with('message',$message)
                ->with('status',$status);
        
    }
}
