<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});
*/


Route::group(['namespace'=>'Front'
	], function(){
	Route::get('/',
			['as' => 'front.home', 
			'uses' => 'FrontController@getInicio']
	);

	Route::get('/categoria/{categoria}',
			['as' => 'front.getCategoria', 
			'uses' => 'FrontController@getCategoria']
	); 


	Route::get('/detalle/{id}',
			['as' => 'front.show', 
			'uses' => 'FrontController@getShow']
	); 

}
);

Route::group(['middleware' => 'sentry.admin', 
				'namespace'=>'Admin'
], function(){
	
	Route::get('/dashboard',
			['as' => 'admin.dashboard', 
			'uses' => 'AdminController@getDashboard']
	);

	Route::get('/getproductos',
			[
				'as' =>'productos.prod',
				'uses' => 'ProductoController@getProductos'
			] );

	Route::put('productos/activa/{id}',
			['as' => 'producto.activaProducto', 
			'uses' => 'ProductoController@activaProducto'
		]);

	Route::resource('productos', 'ProductoController');


	Route::resource('categorias', 'CategoriaController');

}
);
