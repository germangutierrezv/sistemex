-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.1.30-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win32
-- HeidiSQL Versión:             9.1.0.4867
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Volcando estructura de base de datos para sistemex
DROP DATABASE IF EXISTS `sistemex`;
CREATE DATABASE IF NOT EXISTS `sistemex` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `sistemex`;


-- Volcando estructura para tabla sistemex.categoria
DROP TABLE IF EXISTS `categoria`;
CREATE TABLE IF NOT EXISTS `categoria` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla sistemex.categoria: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `categoria` DISABLE KEYS */;
INSERT INTO `categoria` (`id`, `nombre`, `descripcion`, `created_at`, `updated_at`) VALUES
	(1, 'Tom Anderson V', 'Excepturi rem iusto magnam eos aliquam voluptates. Doloribus fugiat sit aut optio. Consequatur voluptate et inventore facere tempore. Qui et non illum eos. Quia consectetur quo maxime mollitia.', '2018-04-29 23:32:17', '2018-04-29 23:32:17'),
	(2, 'Alysha Deckow', 'Quos quia rerum expedita quos. Saepe blanditiis qui perspiciatis. Reprehenderit et quasi assumenda deleniti. Vitae ea voluptatem aspernatur et nam soluta.', '2018-04-29 23:32:17', '2018-04-29 23:32:17'),
	(3, 'Anya Brakus', 'Animi quidem in optio dolor reiciendis. Id tempore velit aut sed quia assumenda perferendis cum. Nostrum dolorem sed sint ea. Expedita voluptatibus dignissimos praesentium vel expedita deserunt.', '2018-04-29 23:32:17', '2018-04-29 23:32:17'),
	(4, 'Tabitha Glover', 'Vel dolorum provident iusto minima. Sint mollitia voluptatibus a harum velit aut itaque. Placeat qui voluptatum et aut.', '2018-04-29 23:32:17', '2018-04-29 23:32:17'),
	(5, 'Dr. Laurel Kirlin II', 'Vel omnis sapiente asperiores qui id architecto sint. Est vel aut doloremque tempora ab. Aut occaecati molestiae est dolor est voluptatibus libero. Id tempore ex optio facilis et ipsam aut.', '2018-04-29 23:32:17', '2018-04-29 23:32:17'),
	(6, 'Emelia Wilkinson', 'Laudantium quasi natus eos atque. Provident voluptatem adipisci qui non et. Quos ipsam et non quia repellendus.', '2018-04-29 23:32:18', '2018-04-29 23:32:18'),
	(7, 'Marquise Kemmer', 'Voluptatem non ea error enim sit. Unde unde a excepturi quia tempore exercitationem. Neque possimus blanditiis et ullam dolores enim optio. Eum in laudantium culpa nam aut.', '2018-04-29 23:32:18', '2018-04-29 23:32:18'),
	(8, 'Mr. Stevie Lesch', 'In veritatis et similique nulla quasi. Libero et quasi est non. Sint eos dicta sunt molestiae eum reiciendis. Sequi soluta ut possimus dolores minima.', '2018-04-29 23:32:18', '2018-04-29 23:32:18'),
	(9, 'Hassan Lockman', 'Numquam fugit nobis et aut velit pariatur. Doloremque atque dolores voluptas natus sunt tenetur error temporibus. Architecto culpa voluptates rerum repellat.', '2018-04-29 23:32:18', '2018-04-29 23:32:18'),
	(10, 'Alec Koepp', 'Dolores commodi eum aut et non ipsum ut. Rerum quisquam provident eum enim facilis. Ullam vitae assumenda dicta et voluptas. Quae optio qui necessitatibus non.', '2018-04-29 23:32:18', '2018-04-29 23:32:18');
/*!40000 ALTER TABLE `categoria` ENABLE KEYS */;


-- Volcando estructura para tabla sistemex.groups
DROP TABLE IF EXISTS `groups`;
CREATE TABLE IF NOT EXISTS `groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `groups_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla sistemex.groups: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;
INSERT INTO `groups` (`id`, `name`, `permissions`, `created_at`, `updated_at`) VALUES
	(1, 'Users', '{"users":1}', '2018-04-25 16:25:33', '2018-04-29 00:50:47'),
	(2, 'Admins', '{"admin":1,"users":1}', '2018-04-25 16:25:33', '2018-04-25 16:25:33'),
	(3, 'Edit', '{"users":1,"admin":1}', NULL, '2018-04-29 00:53:13');
/*!40000 ALTER TABLE `groups` ENABLE KEYS */;


-- Volcando estructura para tabla sistemex.migrations
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla sistemex.migrations: ~9 rows (aproximadamente)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2012_12_06_225921_cartalyst_sentry_install_users', 1),
	(2, '2012_12_06_225929_cartalyst_sentry_install_groups', 1),
	(3, '2012_12_06_225945_cartalyst_sentry_install_users_groups_pivot', 1),
	(4, '2012_12_06_225988_cartalyst_sentry_install_throttle', 1),
	(5, '2014_10_12_000000_create_users_table', 1),
	(6, '2014_10_12_100000_create_password_resets_table', 1),
	(7, '2015_01_14_053439_sentinel_add_username', 1),
	(8, '2018_04_25_122540_create_productos_tables', 1),
	(9, '2018_04_25_122952_create_categorias_tables', 1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;


-- Volcando estructura para tabla sistemex.producto
DROP TABLE IF EXISTS `producto`;
CREATE TABLE IF NOT EXISTS `producto` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` text COLLATE utf8_unicode_ci NOT NULL,
  `precio` decimal(10,2) NOT NULL,
  `estatus` tinyint(1) NOT NULL,
  `imagen` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'default_imagen.png',
  `idcategoria` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla sistemex.producto: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `producto` DISABLE KEYS */;
INSERT INTO `producto` (`id`, `nombre`, `descripcion`, `precio`, `estatus`, `imagen`, `idcategoria`, `created_at`, `updated_at`) VALUES
	(1, 'Prof. Delphia Rolfson', 'Aperiam et quod vero reiciendis nostrum blanditiis et. Porro sapiente omnis dicta in quod.', 80.00, 1, 'default_imagen.png', 9, '2018-04-29 23:32:18', '2018-04-29 23:40:12'),
	(2, 'Darrell Morar', 'Commodi hic et aperiam nihil alias blanditiis impedit voluptatem. Rem beatae eligendi eaque dolor debitis magni. Et molestiae culpa voluptatum molestiae.', 9.60, 0, 'default_imagen.png', 10, '2018-04-29 23:32:18', '2018-04-29 23:32:18'),
	(3, 'Dr. Arden Lowe', 'Beatae dolor reprehenderit nostrum rerum suscipit minima velit. Doloribus dolorem doloribus aliquid incidunt rerum magni quia. Qui illo in voluptas in exercitationem.', 137.60, 1, 'default_imagen.png', 10, '2018-04-29 23:32:18', '2018-04-29 23:32:18'),
	(4, 'Monty Mraz', 'Doloremque delectus ipsa ut necessitatibus. Inventore laboriosam ipsa tenetur. Aliquid velit ea quia. Recusandae et et quia est et excepturi sed.', 48.00, 0, 'default_imagen.png', 10, '2018-04-29 23:32:18', '2018-04-29 23:32:18'),
	(5, 'Declan Baumbach', 'Consequatur harum molestiae ipsum quo consequatur veniam sit. Maxime ad et sint veritatis delectus. In delectus nulla dicta rerum corporis. Voluptatem assumenda perspiciatis quo ad.', 25.60, 1, 'default_imagen.png', 9, '2018-04-29 23:32:18', '2018-04-29 23:32:18'),
	(6, 'Mr. Scot Mertz', 'Sapiente dolorem odit aut vitae et. Molestiae quod sint deserunt. Odit et itaque dolorem quibusdam aut dolor atque.', 118.40, 0, 'default_imagen.png', 3, '2018-04-29 23:32:18', '2018-04-29 23:32:18'),
	(7, 'Cordie Lind Jr.', 'Accusantium quasi asperiores est hic delectus. Sit et quis magnam. Corrupti quia repellat illum ratione maxime. Adipisci atque quidem optio repellat.', 147.20, 1, 'default_imagen.png', 10, '2018-04-29 23:32:18', '2018-04-29 23:32:18'),
	(8, 'Flo Swift', 'Aliquam voluptatem inventore suscipit ea sit. Nihil ut quas aut est distinctio ex non. Cupiditate et nesciunt sequi reprehenderit et ut voluptate voluptas.', 115.20, 1, 'Flo Swift.png', 3, '2018-04-29 23:32:18', '2018-04-29 23:57:24'),
	(9, 'Kennedy Stracke', 'Et nobis ut ullam earum. Repellat nihil in veritatis laudantium quibusdam. Amet exercitationem numquam veniam odit optio ipsum.', 112.00, 1, 'default_imagen.png', 10, '2018-04-29 23:32:18', '2018-04-29 23:32:18'),
	(10, 'Genesis Ernser', 'Velit iure facere perspiciatis reiciendis. Fugit sit id veniam quia aut amet asperiores. Eius culpa illo pariatur itaque autem voluptate.', 6.40, 1, 'default_imagen.png', 3, '2018-04-29 23:32:18', '2018-04-29 23:32:18'),
	(11, 'Onie Roob', 'Et facilis magni facere voluptatem quam illum alias soluta. Accusamus qui enim inventore ea consequatur. Et nemo voluptas hic facilis cupiditate possimus. Est sint consequatur perspiciatis sed.', 60.80, 1, 'default_imagen.png', 6, '2018-04-29 23:32:18', '2018-04-29 23:32:18'),
	(12, 'Tom Medhurst Sr.', 'Non voluptatem accusantium omnis nam possimus. Sed error velit cum. Veniam vel vero error expedita laborum ex quo. Ut numquam sint eaque molestiae ullam incidunt ipsum.', 57.60, 1, 'default_imagen.png', 10, '2018-04-29 23:32:18', '2018-04-29 23:32:18'),
	(13, 'Judd Stokes', 'Rerum exercitationem quia sint voluptates fuga qui at. Nisi omnis id aliquam ea qui. Sed doloribus impedit tenetur qui.', 128.00, 0, 'default_imagen.png', 4, '2018-04-29 23:32:18', '2018-04-29 23:32:18'),
	(14, 'Dorcas Schaden', 'Sit sint veniam ea ea corporis. Autem delectus in voluptas repudiandae. Molestiae beatae sit voluptate animi et quaerat. Doloribus molestiae aliquid aut excepturi aut cupiditate consequatur.', 28.80, 0, 'default_imagen.png', 8, '2018-04-29 23:32:18', '2018-04-29 23:32:18'),
	(15, 'Arianna Dickens', 'Iusto sit quam ducimus qui maiores consequuntur. Qui est sapiente dolor. Quis placeat velit qui cum.', 60.80, 0, 'default_imagen.png', 4, '2018-04-29 23:32:18', '2018-04-29 23:32:18'),
	(16, 'Prof. Orpha Armstrong MD', 'Qui quaerat dicta assumenda voluptas explicabo officiis. Eaque numquam et necessitatibus sapiente. Aut tempora et excepturi consequuntur.', 12.80, 1, 'default_imagen.png', 3, '2018-04-29 23:32:18', '2018-04-29 23:32:18'),
	(17, 'Felicita Hegmann', 'Adipisci nulla dignissimos deserunt error dolorum. Et qui eum nesciunt quasi qui.', 156.80, 1, 'Felicita Hegmann.png', 2, '2018-04-29 23:32:18', '2018-04-29 23:55:19'),
	(18, 'Dr. Bart Rowe V', 'Quibusdam quas deleniti asperiores. In illum enim pariatur quos labore sit consequatur. Minus quia voluptatem ut facere laudantium saepe.', 89.60, 0, 'default_imagen.png', 10, '2018-04-29 23:32:18', '2018-04-29 23:32:18'),
	(19, 'Dr. Velma Satterfield', 'Nihil quo quo odit modi. Repudiandae ut eveniet nostrum velit excepturi aliquam ut. Tenetur earum quo et laborum. Ipsam est omnis ex delectus saepe.', 147.20, 1, 'default_imagen.png', 6, '2018-04-29 23:32:18', '2018-04-29 23:32:18'),
	(20, 'Brenna Doyle', 'Ab quia rem eaque numquam. Minima est sunt vitae fugiat et sint.', 32.00, 1, 'default_imagen.png', 1, '2018-04-29 23:32:19', '2018-04-29 23:32:19'),
	(21, 'Imani Robel', 'Optio dolorem deleniti enim unde deleniti dolorum. Qui quae repellendus eos dolorum velit occaecati. Accusantium velit accusamus at quam.', 115.20, 0, 'default_imagen.png', 1, '2018-04-29 23:32:19', '2018-04-29 23:32:19'),
	(22, 'Luciano Adams', 'Est ad enim non rerum rem nisi. Recusandae et eos dolor aliquam enim sed consequatur. Et magnam eum in ad.', 147.20, 1, 'default_imagen.png', 10, '2018-04-29 23:32:19', '2018-04-29 23:32:19'),
	(23, 'Robb Gaylord', 'Aut aut cumque nesciunt ut quisquam. Inventore ex impedit ad qui voluptatibus eum. Qui porro deserunt est quia aut sapiente. Eos eveniet cumque labore ea.', 28.80, 1, 'default_imagen.png', 1, '2018-04-29 23:32:19', '2018-04-29 23:32:19'),
	(24, 'Ottilie Connelly', 'Architecto enim velit quibusdam tempore nam velit maiores architecto. Error ut id sit quos et aut. Facilis in alias consequatur quidem et qui omnis est.', 156.80, 1, 'default_imagen.png', 5, '2018-04-29 23:32:19', '2018-04-29 23:32:19'),
	(25, 'Amy Lemke', 'Placeat maiores debitis est. Incidunt mollitia quasi exercitationem aliquam harum qui qui. Qui ad a sed.', 128.00, 1, 'default_imagen.png', 9, '2018-04-29 23:32:19', '2018-04-29 23:32:19'),
	(26, 'Mr. Adolfo Schmitt', 'Repudiandae maiores omnis temporibus. Ea placeat voluptate et. Alias rerum sunt odit quo perferendis nihil.', 153.60, 0, 'default_imagen.png', 8, '2018-04-29 23:32:19', '2018-04-29 23:32:19'),
	(27, 'Florian Wilderman', 'Expedita illo quia maxime placeat placeat ut eos. Voluptate excepturi ipsam voluptatum est optio doloremque. Ad omnis voluptatem facilis excepturi.', 144.00, 1, 'default_imagen.png', 8, '2018-04-29 23:32:19', '2018-04-29 23:32:19'),
	(28, 'Dr. Gudrun Kertzmann', 'Laboriosam ducimus facilis minima molestias dolore. Vel et aliquid consequatur molestiae voluptas repudiandae. Atque accusantium laborum ipsa sunt iste temporibus. Ut eius sit fuga magnam quis.', 105.60, 0, 'default_imagen.png', 5, '2018-04-29 23:32:19', '2018-04-29 23:32:19'),
	(29, 'Prof. Astrid Corwin DVM', 'Architecto perspiciatis modi qui eveniet. Possimus nulla provident perspiciatis officia dolor exercitationem officia. Ut in ut ipsam dicta.', 124.80, 1, 'default_imagen.png', 6, '2018-04-29 23:32:19', '2018-04-29 23:32:19'),
	(30, 'Mr. Brannon Hodkiewicz DVM', 'Laborum ut doloremque nisi itaque sit consequatur harum. Esse officiis quis enim excepturi omnis quo consequatur nostrum. Quos vel voluptatem aspernatur dolores quae voluptatem.', 60.80, 1, 'default_imagen.png', 9, '2018-04-29 23:32:19', '2018-04-29 23:32:19'),
	(31, 'Prof. Jeanette Kautzer', 'Et incidunt nostrum hic eligendi ipsa. Neque veritatis amet eaque inventore commodi dolorum quae. Sed ex dicta velit temporibus harum voluptas error.', 102.40, 1, 'default_imagen.png', 6, '2018-04-29 23:32:19', '2018-04-29 23:32:19'),
	(32, 'Prof. Dora Deckow PhD', 'Ipsum quisquam minus temporibus id. Quas voluptatem ipsa assumenda at quibusdam atque. In hic molestiae rerum in sed esse. Suscipit ab tenetur porro omnis.', 83.20, 0, 'default_imagen.png', 10, '2018-04-29 23:32:19', '2018-04-29 23:32:19'),
	(33, 'Mrs. Rebekah Kuhic PhD', 'Ratione molestiae neque ut ducimus. Et enim vitae odio est nulla. Nihil culpa voluptate fugit eos.', 86.40, 0, 'default_imagen.png', 10, '2018-04-29 23:32:19', '2018-04-29 23:32:19'),
	(34, 'Jamarcus Corwin', 'Sit impedit voluptate sit porro temporibus deleniti. Nesciunt et non molestiae neque eligendi. Dolorem porro voluptas velit.', 9.60, 0, 'default_imagen.png', 2, '2018-04-29 23:32:19', '2018-04-29 23:32:19'),
	(35, 'Felicita Carter', 'Dolorem debitis quis sint corrupti. Et alias ex atque hic nesciunt. Quam velit laboriosam ex. Inventore eos commodi veniam aperiam explicabo.', 25.60, 0, 'default_imagen.png', 2, '2018-04-29 23:32:19', '2018-04-29 23:32:19'),
	(36, 'Amya Becker MD', 'Officiis et eum consequatur consequatur id laboriosam. Dolor vel officia sit ut atque qui rerum.', 48.00, 1, 'default_imagen.png', 1, '2018-04-29 23:32:19', '2018-04-29 23:32:19'),
	(37, 'Valentina Oberbrunner', 'Asperiores atque iusto et molestias ipsa perspiciatis sint. Cumque vitae est culpa voluptas quia ullam perferendis. Qui nulla aut neque possimus perspiciatis. Hic iure sunt quia vel sapiente.', 96.00, 1, 'default_imagen.png', 1, '2018-04-29 23:32:19', '2018-04-29 23:32:19'),
	(38, 'Jeanie Lind', 'Omnis possimus nihil voluptatem dolorum voluptatibus unde. Corporis quas vitae nobis quis sapiente. Eligendi est sint ullam sint odit. Quia et ut et inventore sit quibusdam.', 67.20, 0, 'default_imagen.png', 4, '2018-04-29 23:32:19', '2018-04-29 23:32:19'),
	(39, 'Brannon West', 'Modi tempora velit vel molestiae sunt quia ullam id. Voluptatem fuga fuga dolorem alias odit. Sed est dolorem qui et dolor est. Fuga libero quasi non molestiae.', 89.60, 1, 'default_imagen.png', 8, '2018-04-29 23:32:19', '2018-04-29 23:32:19'),
	(40, 'Dr. Laury Koch', 'Qui minima mollitia incidunt enim magni ex. Facilis voluptates dolorem qui ducimus necessitatibus ea accusamus.', 19.20, 0, 'default_imagen.png', 4, '2018-04-29 23:32:19', '2018-04-29 23:32:19'),
	(41, 'Kailey Gusikowski', 'Ea vero at similique. In itaque sed non quod. Minus voluptatibus omnis saepe eos et dolores expedita. Voluptate odit nihil maiores quo amet necessitatibus excepturi. Iusto inventore nulla laboriosam.', 112.00, 0, 'default_imagen.png', 1, '2018-04-29 23:32:19', '2018-04-29 23:32:19'),
	(42, 'Niko Schaden', 'Consequatur aliquid non quo odit est dolore. Atque nemo magni temporibus. Quae illum ducimus minima. Eos laboriosam sed quia minima id sit et.', 156.80, 0, 'default_imagen.png', 4, '2018-04-29 23:32:19', '2018-04-29 23:32:19'),
	(43, 'Ciara O\'Conner', 'Ut nemo amet animi porro est qui ut. Distinctio dolores nostrum et recusandae impedit atque nostrum est. Laudantium sit sed aut sunt.', 6.40, 0, 'default_imagen.png', 5, '2018-04-29 23:32:19', '2018-04-29 23:32:19'),
	(44, 'Sherwood Rosenbaum', 'Dolores sed tempora sit maiores asperiores. Voluptatem totam quis eveniet velit debitis tempora neque aut. Non odio aut cumque in rem illo laboriosam suscipit.', 25.60, 1, 'default_imagen.png', 7, '2018-04-29 23:32:19', '2018-04-29 23:32:19'),
	(45, 'Jay Keebler', 'Debitis voluptate voluptatem sit minima sed pariatur. Dolorum suscipit quas aut iste quia unde accusantium. Illo voluptas aut fugiat non. Illum ut quia sapiente dolorum non qui.', 108.80, 1, 'default_imagen.png', 9, '2018-04-29 23:32:20', '2018-04-29 23:32:20'),
	(46, 'Alison Jacobi', 'Et officia est qui ratione neque. Voluptate enim totam voluptatum. Optio autem consequatur porro omnis aut dolor illo autem.', 38.40, 1, 'default_imagen.png', 5, '2018-04-29 23:32:20', '2018-04-29 23:32:20'),
	(47, 'Myrtle Denesik V', 'Dolorem non quam id et facere neque sit facere. Cumque maxime dicta voluptatem quia omnis itaque consequatur.', 9.60, 1, 'default_imagen.png', 2, '2018-04-29 23:32:20', '2018-04-29 23:32:20'),
	(48, 'Kellen Mueller', 'Aut est est laborum perferendis ea ducimus alias. Optio impedit illo quia ea nobis et cum.', 144.00, 1, 'default_imagen.png', 5, '2018-04-29 23:32:20', '2018-04-29 23:32:20'),
	(49, 'Ahmad Will', 'Consequatur impedit nam aut ipsam. Totam voluptates aut inventore alias.', 128.00, 1, 'default_imagen.png', 7, '2018-04-29 23:32:20', '2018-04-29 23:32:20'),
	(50, 'Harmon Price DVM', 'Sint sequi saepe minima cumque. Ut ut aliquam provident. Blanditiis animi quia ipsum sit reiciendis qui quam. Cupiditate blanditiis doloribus molestiae error voluptatem velit est est.', 9.60, 0, 'default_imagen.png', 2, '2018-04-29 23:32:20', '2018-04-29 23:32:20'),
	(51, 'Erich Fahey', 'Et saepe sed voluptas ab adipisci sit asperiores quis. Soluta nam pariatur in iusto sunt impedit error. In rerum soluta veniam est. Debitis aut ad adipisci et sed qui architecto ut.', 41.60, 0, 'default_imagen.png', 3, '2018-04-29 23:32:20', '2018-04-29 23:32:20'),
	(52, 'Jamar Runolfsdottir', 'Ipsum necessitatibus eligendi sit eius. Omnis optio eligendi cupiditate veniam distinctio delectus. Quia et consequuntur veritatis accusamus. Molestias beatae laboriosam corrupti laborum dolorum ut.', 35.20, 1, 'default_imagen.png', 2, '2018-04-29 23:32:20', '2018-04-29 23:32:20'),
	(53, 'Charlotte Larson', 'Ad est enim debitis doloremque consequatur praesentium illum. Nemo praesentium eligendi et officiis eaque sequi quo. Qui est quis ducimus ipsam sapiente impedit.', 96.00, 1, 'default_imagen.png', 5, '2018-04-29 23:32:20', '2018-04-29 23:32:20'),
	(54, 'Emiliano Murphy Jr.', 'Quis ad aperiam ut non accusantium recusandae. Consequatur aliquam alias illum aut in soluta. Optio a occaecati soluta dolor vero est ab. Rerum molestiae a omnis eaque vel.', 118.40, 0, 'default_imagen.png', 8, '2018-04-29 23:32:20', '2018-04-29 23:32:20'),
	(55, 'Prof. Rickey Witting V', 'Molestiae quae cumque et. Ut corporis consequatur ea labore rem quia. Quia dicta quibusdam optio tempora molestiae quis. Culpa exercitationem repellendus veniam eaque rerum.', 54.40, 0, 'default_imagen.png', 8, '2018-04-29 23:32:20', '2018-04-29 23:32:20'),
	(56, 'Prof. Abe McClure Jr.', 'Nobis illo animi sapiente. Sit qui ut sit corporis libero modi nisi. Illo aut illum vitae. Adipisci corporis occaecati occaecati quidem.', 44.80, 1, 'default_imagen.png', 8, '2018-04-29 23:32:20', '2018-04-29 23:32:20'),
	(57, 'Janick Wolff', 'Nostrum enim fuga ea magnam rem. Ut et cum blanditiis animi quam sit consequatur sapiente. Quos eos porro dolorum quia asperiores facilis.', 96.00, 1, 'default_imagen.png', 8, '2018-04-29 23:32:20', '2018-04-29 23:32:20'),
	(58, 'Maximo Romaguera', 'Et aut eos minus debitis at cupiditate. Quia iusto consequatur nihil totam minus nostrum.', 156.80, 1, 'default_imagen.png', 3, '2018-04-29 23:32:20', '2018-04-29 23:32:20'),
	(59, 'Alberto Welch', 'Est ut perferendis aut qui tempore molestiae velit. Non ut consequatur id. Sunt deleniti inventore veritatis sint exercitationem aut excepturi amet.', 9.60, 1, 'default_imagen.png', 6, '2018-04-29 23:32:20', '2018-04-29 23:32:20'),
	(60, 'Jean Brakus', 'Non esse voluptatem quibusdam ducimus omnis dolorem. Vitae ex quis cum laborum quam itaque incidunt. Accusantium aut non doloribus iusto repellendus.', 67.20, 1, 'default_imagen.png', 8, '2018-04-29 23:32:20', '2018-04-29 23:32:20'),
	(61, 'Miss Mattie Kris', 'Est magni eos dolores quae impedit nisi. Ratione earum at qui consectetur dolore. Dolores accusantium repellendus aut ratione. Similique enim quas expedita nam quibusdam ut.', 144.00, 1, 'default_imagen.png', 2, '2018-04-29 23:32:20', '2018-04-29 23:32:20'),
	(62, 'Cameron Mayer', 'Voluptate ab ut sit ut. Ullam reprehenderit aut odio consectetur eaque. Ducimus voluptates deserunt et est.', 67.20, 0, 'default_imagen.png', 2, '2018-04-29 23:32:20', '2018-04-29 23:32:20'),
	(63, 'Diana Kuvalis', 'Itaque et perferendis voluptas similique aut reiciendis. Et autem est similique. Porro aut aut fuga quam eos. Laudantium libero ad fugiat.', 140.80, 0, 'default_imagen.png', 2, '2018-04-29 23:32:20', '2018-04-29 23:32:20'),
	(64, 'Sandrine Stoltenberg', 'Quam et id facere qui debitis libero. Animi vitae enim omnis libero. Veritatis excepturi odit incidunt ipsam odio aspernatur. Suscipit consequuntur est magni aut quia. Accusantium error ut tenetur.', 108.80, 1, 'default_imagen.png', 9, '2018-04-29 23:32:20', '2018-04-29 23:32:20'),
	(65, 'Kirk Windler', 'Facere illo omnis et reprehenderit earum. Id aut hic repudiandae excepturi neque aperiam et. Rerum occaecati vel aut repellendus. Quisquam rem eaque nesciunt accusantium sunt earum.', 115.20, 0, 'default_imagen.png', 8, '2018-04-29 23:32:20', '2018-04-29 23:32:20'),
	(66, 'Alexandrea Nicolas', 'Et voluptate aut voluptates asperiores dolores quisquam. Libero ea quia nemo dolores impedit quidem.', 28.80, 0, 'default_imagen.png', 9, '2018-04-29 23:32:20', '2018-04-29 23:32:20'),
	(67, 'Prof. Herbert Mayer V', 'Est commodi et aliquam. Facilis voluptas est harum minima aspernatur est. Consequatur quia voluptatum quos accusantium iusto et asperiores beatae.', 9.60, 1, 'default_imagen.png', 3, '2018-04-29 23:32:20', '2018-04-29 23:32:20'),
	(68, 'Mr. Isom Weber III', 'Corporis ut hic sit accusantium voluptas. Nulla ut quia doloremque qui esse ipsum fuga.', 89.60, 1, 'default_imagen.png', 6, '2018-04-29 23:32:20', '2018-04-29 23:32:20'),
	(69, 'Howard Hermann', 'Excepturi ipsum velit nulla illo corrupti earum. Velit voluptatem illum distinctio. Est harum aut qui dolores laudantium consequatur in. Ut voluptate itaque consequuntur consequatur nesciunt.', 134.40, 1, 'default_imagen.png', 4, '2018-04-29 23:32:20', '2018-04-29 23:32:20'),
	(70, 'Prof. Leo Smith III', 'Rerum sit sint qui ducimus in vel fugit eum. Animi eveniet perspiciatis autem. Voluptatum minima doloribus illo sed et. Qui quam iure ea minima illum. Iure omnis minima voluptatem.', 156.80, 0, 'default_imagen.png', 10, '2018-04-29 23:32:20', '2018-04-29 23:32:20'),
	(71, 'Garth Witting III', 'Dolores iure temporibus recusandae. Et qui eos voluptatibus ut quaerat. Illum optio quo nihil sit eum mollitia non. Beatae voluptatibus dolorem praesentium sint.', 96.00, 0, 'default_imagen.png', 7, '2018-04-29 23:32:20', '2018-04-29 23:32:20'),
	(72, 'Miss Hilma Spencer', 'Facere incidunt aut aut aut. Enim amet suscipit minima non itaque aliquam. Consequatur fugit adipisci inventore et mollitia explicabo quaerat.', 19.20, 1, 'default_imagen.png', 9, '2018-04-29 23:32:20', '2018-04-29 23:32:20'),
	(73, 'Chance Bartell', 'Quae fugit dicta nam aut. Ab distinctio voluptatem omnis incidunt molestiae. Ut ut sed eligendi voluptas distinctio placeat ullam. Explicabo dolorem fuga dolorem voluptas explicabo.', 153.60, 0, 'default_imagen.png', 9, '2018-04-29 23:32:20', '2018-04-29 23:32:20'),
	(74, 'Henderson Jacobi', 'Mollitia ea molestiae et quod velit dolor eveniet. Illum cum modi qui magni aut asperiores. Ex vel ipsa ipsum quia error natus.', 60.80, 1, 'default_imagen.png', 9, '2018-04-29 23:32:21', '2018-04-29 23:32:21'),
	(75, 'Leonardo Bradtke DDS', 'Quisquam harum et quidem vero et nemo quia modi. Enim tempore harum asperiores illum. Optio omnis quod et voluptas aut aut cupiditate ex. Corrupti repudiandae nemo sed maxime nihil.', 48.00, 1, 'default_imagen.png', 2, '2018-04-29 23:32:21', '2018-04-29 23:32:21'),
	(76, 'Betsy Bahringer', 'Impedit quae totam repudiandae animi eius. Est rem quis sit aperiam quas eius natus.', 3.20, 0, 'default_imagen.png', 4, '2018-04-29 23:32:21', '2018-04-29 23:32:21'),
	(77, 'Dr. Deontae Mertz', 'Ut vitae reprehenderit suscipit velit et voluptatem. Sed ratione tenetur magni quos. Et id deserunt voluptate tenetur est rerum.', 128.00, 0, 'default_imagen.png', 3, '2018-04-29 23:32:21', '2018-04-29 23:32:21'),
	(78, 'Jarred Roberts DDS', 'Voluptatum et velit omnis et molestias. Dignissimos eligendi consequuntur rerum molestiae vel. Pariatur dolores quia tempora unde accusantium sed incidunt.', 32.00, 1, 'default_imagen.png', 10, '2018-04-29 23:32:21', '2018-04-29 23:32:21'),
	(79, 'Dr. Saul Nienow V', 'Doloremque aut iste quo doloremque dolores praesentium atque. Ipsa praesentium mollitia minus rerum vel. Eum voluptatem temporibus laudantium dolores cumque suscipit eligendi.', 25.60, 0, 'default_imagen.png', 3, '2018-04-29 23:32:21', '2018-04-29 23:32:21'),
	(80, 'Itzel Feil', 'Voluptatem et excepturi ut libero. Et rerum ea magnam ducimus officiis. Ut eaque vero dolorem qui tempora quod ex velit.', 35.20, 0, 'default_imagen.png', 1, '2018-04-29 23:32:21', '2018-04-29 23:32:21'),
	(81, 'Kaelyn Metz', 'Et voluptas rerum corrupti. Et non quam ut quo aut consequatur quis. A alias aut aspernatur incidunt.', 147.20, 1, 'default_imagen.png', 9, '2018-04-29 23:32:21', '2018-04-29 23:32:21'),
	(82, 'Reece Anderson', 'Et praesentium tenetur similique aut. Quia dolorum non natus magni rem et dolores et.', 41.60, 1, 'default_imagen.png', 6, '2018-04-29 23:32:21', '2018-04-29 23:32:21'),
	(83, 'Nestor Lebsack', 'Ipsum ipsam quibusdam facilis architecto commodi fugit. Mollitia dolore omnis quaerat temporibus. Iusto tempora deserunt temporibus fugiat. Dignissimos odit voluptatem eaque quisquam aut.', 147.20, 0, 'default_imagen.png', 1, '2018-04-29 23:32:21', '2018-04-29 23:32:21'),
	(84, 'Mrs. Melba Kulas DVM', 'Et itaque suscipit ab architecto repellat aut fugiat. At accusamus eligendi maiores sint. Labore est veniam eius corporis quo enim.', 99.20, 1, 'default_imagen.png', 4, '2018-04-29 23:32:21', '2018-04-29 23:32:21'),
	(85, 'Kaycee Braun', 'Ea aut illo sequi est ut dolorem provident. Fuga repudiandae cumque eum et similique aperiam est. Et quia itaque dolorum temporibus quae ea.', 115.20, 0, 'default_imagen.png', 10, '2018-04-29 23:32:21', '2018-04-29 23:32:21'),
	(86, 'Dr. Walker Kemmer Sr.', 'Dolor soluta et facere accusamus recusandae consequatur. Autem quos libero est aspernatur rem. Qui id ex praesentium laudantium. Aut velit aut aut eligendi. Unde soluta magni sed sunt consectetur.', 144.00, 1, 'default_imagen.png', 4, '2018-04-29 23:32:21', '2018-04-29 23:32:21'),
	(87, 'Harmony Hettinger', 'Officiis ratione neque voluptate officiis nulla. Voluptatibus doloremque ratione aut excepturi corporis pariatur. Et aut non tempore molestias.', 83.20, 0, 'default_imagen.png', 8, '2018-04-29 23:32:21', '2018-04-29 23:32:21'),
	(88, 'Brooklyn Mosciski', 'Enim mollitia ut quas veritatis dolores. Ut est illum iure deleniti nulla eos dicta magni. Aliquam et vel eos unde.', 134.40, 1, 'default_imagen.png', 4, '2018-04-29 23:32:21', '2018-04-29 23:32:21'),
	(89, 'Thalia Miller Sr.', 'Voluptas asperiores dolorem ad. Nihil aut id atque enim repellendus quam et. Labore error qui culpa eaque. Cupiditate dolorem ex nulla hic.', 83.20, 0, 'default_imagen.png', 9, '2018-04-29 23:32:21', '2018-04-29 23:32:21'),
	(90, 'Dr. Mckenzie Beatty', 'Fugit dignissimos sed pariatur porro ea assumenda amet est. Reiciendis rem nihil nemo doloremque. Cum sit blanditiis sint et.', 108.80, 0, 'default_imagen.png', 5, '2018-04-29 23:32:21', '2018-04-29 23:32:21'),
	(91, 'Mekhi Greenholt DVM', 'Iste sapiente qui quaerat temporibus quod. Et consequatur nesciunt commodi excepturi pariatur cumque. Perspiciatis impedit similique qui amet.', 99.20, 1, 'default_imagen.png', 2, '2018-04-29 23:32:21', '2018-04-29 23:32:21'),
	(92, 'Mr. Josiah Howell MD', 'Omnis nam maxime aut nisi harum voluptatem. Eveniet deleniti ipsa ad ut atque. Qui quia molestiae praesentium illo autem. Iste illum nesciunt autem sint qui.', 99.20, 0, 'default_imagen.png', 1, '2018-04-29 23:32:21', '2018-04-29 23:32:21'),
	(93, 'Prof. Domingo Wehner', 'Quo voluptatem eaque itaque ut. Soluta sint nemo enim voluptatem. Inventore accusantium debitis blanditiis placeat temporibus aut. Est ipsum quis assumenda dolor.', 80.00, 1, 'default_imagen.png', 5, '2018-04-29 23:32:21', '2018-04-29 23:32:21'),
	(94, 'Mr. Rashawn Cremin Sr.', 'Perspiciatis enim praesentium exercitationem assumenda. Vel labore quidem odio distinctio eum voluptatibus illum. Ut enim at nulla ea.', 19.20, 1, 'default_imagen.png', 6, '2018-04-29 23:32:21', '2018-04-29 23:32:21'),
	(95, 'Prof. Marty Kertzmann III', 'Ut voluptatum qui eum et facilis. Voluptas assumenda nihil autem consequatur qui odio eos. Est sit earum voluptas tempore. At eaque eum doloribus aut quia sint.', 140.80, 0, 'default_imagen.png', 3, '2018-04-29 23:32:21', '2018-04-29 23:32:21'),
	(96, 'Juvenal Howe', 'Autem quaerat quia quia similique debitis dolor. Amet quod voluptatem harum ipsum. Alias magnam non sunt voluptatum quibusdam.', 160.00, 0, 'default_imagen.png', 3, '2018-04-29 23:32:21', '2018-04-29 23:32:21'),
	(97, 'Wade Paucek Sr.', 'Odio laudantium corrupti adipisci quidem explicabo eaque quia. Vitae perferendis illum et et cumque tempora iure.', 147.20, 0, 'default_imagen.png', 1, '2018-04-29 23:32:21', '2018-04-29 23:32:21'),
	(98, 'Pinkie Brown', 'Et id alias natus exercitationem id. Dolorem quam in eius eos labore expedita nam. Expedita consequatur molestiae aut quo. Voluptatem voluptatem aspernatur rem autem saepe et optio.', 51.20, 1, 'default_imagen.png', 3, '2018-04-29 23:32:21', '2018-04-29 23:32:21'),
	(99, 'Jerod Block II', 'Provident sequi saepe magnam placeat quo unde ipsum. Placeat id aperiam commodi nihil corporis.', 147.20, 0, 'default_imagen.png', 10, '2018-04-29 23:32:21', '2018-04-29 23:32:21'),
	(100, 'Trudie Cronin IV', 'Fugit ducimus sunt dolores ut. Non vel quo aut laudantium atque qui soluta. Accusamus cupiditate quis ducimus quia nesciunt et.', 38.40, 1, 'default_imagen.png', 4, '2018-04-29 23:32:21', '2018-04-29 23:32:21'),
	(101, 'Producto 01', 'Este es el producto 01', 0.06, 1, 'Producto 01.jpg', 9, '2018-04-30 00:01:23', '2018-04-30 00:01:23');
/*!40000 ALTER TABLE `producto` ENABLE KEYS */;


-- Volcando estructura para tabla sistemex.throttle
DROP TABLE IF EXISTS `throttle`;
CREATE TABLE IF NOT EXISTS `throttle` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `ip_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attempts` int(11) NOT NULL DEFAULT '0',
  `suspended` tinyint(1) NOT NULL DEFAULT '0',
  `banned` tinyint(1) NOT NULL DEFAULT '0',
  `last_attempt_at` timestamp NULL DEFAULT NULL,
  `suspended_at` timestamp NULL DEFAULT NULL,
  `banned_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `throttle_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla sistemex.throttle: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `throttle` DISABLE KEYS */;
INSERT INTO `throttle` (`id`, `user_id`, `ip_address`, `attempts`, `suspended`, `banned`, `last_attempt_at`, `suspended_at`, `banned_at`) VALUES
	(1, 1, '::1', 0, 0, 0, NULL, NULL, '2018-04-29 00:08:30'),
	(2, 3, NULL, 0, 0, 0, NULL, NULL, NULL),
	(3, 2, '::1', 0, 0, 0, '2018-04-29 19:17:45', NULL, NULL);
/*!40000 ALTER TABLE `throttle` ENABLE KEYS */;


-- Volcando estructura para tabla sistemex.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8_unicode_ci,
  `activated` tinyint(1) NOT NULL DEFAULT '0',
  `activation_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `activated_at` timestamp NULL DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `persist_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reset_password_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  UNIQUE KEY `users_username_unique` (`username`),
  KEY `users_activation_code_index` (`activation_code`),
  KEY `users_reset_password_code_index` (`reset_password_code`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla sistemex.users: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `email`, `password`, `permissions`, `activated`, `activation_code`, `activated_at`, `last_login`, `persist_code`, `reset_password_code`, `first_name`, `last_name`, `created_at`, `updated_at`, `username`) VALUES
	(1, 'admin@admin.com', '$2y$10$kW3OZ/KYpXwBJ5Hrzdz2GOPbpG3rexyCmZQVMIClDe2i5hL1/tF9i', NULL, 1, NULL, NULL, '2018-04-29 23:23:56', '$2y$10$QowoRcx46XVLT5tYqApYEeXyADw8pb8i1HflxnXjdwtHM.5CTjKyW', NULL, 'Administrador', 'Administrador', '2018-04-25 16:25:33', '2018-04-29 23:23:56', 'admin'),
	(2, 'user@user.com', '$2y$10$x6.sALgcpmy9g1JeLh2r0Oz8FalfCOaK9gWhBSeoEokRRNXno6sIC', NULL, 0, NULL, NULL, '2018-04-29 23:42:03', '$2y$10$y/hfA/Luv0fr92JeKQ36T.4bsOkl46xktPXDnqm4pp7nY/l1TtY4u', NULL, NULL, NULL, '2018-04-25 16:25:33', '2018-04-29 23:42:03', 'user');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;


-- Volcando estructura para tabla sistemex.users_groups
DROP TABLE IF EXISTS `users_groups`;
CREATE TABLE IF NOT EXISTS `users_groups` (
  `user_id` int(10) unsigned NOT NULL,
  `group_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla sistemex.users_groups: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `users_groups` DISABLE KEYS */;
INSERT INTO `users_groups` (`user_id`, `group_id`) VALUES
	(1, 1),
	(1, 2),
	(2, 1);
/*!40000 ALTER TABLE `users_groups` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
