<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class CategoriasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       	$faker= Faker::create();
        for($j=1; $j<=10; $j++)
        {
            $p=new App\Models\Categorias();
            $p->nombre=$faker->name;
            $p->descripcion=$faker->text;
            
            $p->save();
        }
    }
    
}
