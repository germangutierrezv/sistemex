<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;


class ProductosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Models\Productos::truncate();

        $faker= Faker::create();
        for($j=1; $j<=100; $j++)
        {
            $p=new App\Models\Productos();
            $p->nombre=$faker->name;
            $p->descripcion=$faker->text;
            $p->precio=mt_rand(1,50)*3.2;
            $p->estatus=$faker->randomElement(['0','1']);
            $p->idcategoria=mt_rand(1,10);
            
            $p->save();
            
        } 

    }
}
