@extends('layouts.admin')

@section('title', 'Crear Categoria')
@section('value_nav_cat', 'active')

@section('content')
	<div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h2 class="panel-title"><i class="fa fa-plus fa-fw"></i> Nueva Categoria</h2>
            </div>
            <div class="panel-body">

            @include('notifications.status_message')
            @include('notifications.errors_message')

            <form action="{{ url('categorias') }}" method="POST" class="form-horizontal">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">

				<div class="form-group{{ $errors->has('nombre') ? ' has-error' : '' }} has-feedback">
					<label for="cedula" class="col-md-3 control-label">
						Nombre
					</label>	
						<div class="col-md-6">
							<input type="text" class="form-control" name="nombre" id="nombre" value="{{ old('nombre') }}" required>
						</div>
				</div>
				<div class="form-group">
					<label for="descripcion" class="col-md-3 control-label">
						Descripción
					</label>	
						<div class="col-md-6">
							<textarea class="form-control" name="descripcion" id="descripcion" rows="2">{{ old('descripcion') }}
							</textarea>
						</div>
				</div>
				<div class="form-group">
					<div class="col-md-3 col-md-offset-4">
						<a href="{{ url()->previous() }}" class="btn btn-primary"><i class="fa fa-chevron-left"></i> Volver</a>
						<button type="submit" class="btn btn-success btn-md"><i class="fa fa-plus"></i>&nbsp; Agregar</button>
					</div>
				</div>
			
			</form>

            </div>
        </div>
	</div>
@endsection
