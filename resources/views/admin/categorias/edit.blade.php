@extends('layouts.admin')

@section('title', 'Editar Categoria')
@section('value_nav_cat', 'active')

@section('content')
	<div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h2 class="panel-title"><i class="fa fa-plus fa-fw"></i> Editar Categoria</h2>
            </div>
            <div class="panel-body">

            @include('notifications.status_message')
            @include('notifications.errors_message')

            <form action="{{ url('categorias',$categoria->id) }}" method="POST" class="form-horizontal">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" name="_method" value="PUT">

				<div class="form-group{{ $errors->has('nombre') ? ' has-error' : '' }} has-feedback">
					<label for="cedula" class="col-md-3 control-label">
						Nombre
					</label>	
						<div class="col-md-6">
							<input type="text" class="form-control" name="nombre" id="nombre" value="{{ $categoria->nombre }}" required>
						</div>
				</div>
				<div class="form-group">
					<label for="descripcion" class="col-md-3 control-label">
						Descripción
					</label>	
						<div class="col-md-6">
							<textarea class="form-control" name="descripcion" id="descripcion" rows="2">{{ $categoria->descripcion }}
							</textarea>
						</div>
				</div>
				<div class="form-group">
					<div class="col-md-3 col-md-offset-4">
						<a href="{{ url()->previous() }}" class="btn btn-primary"><i class="fa fa-chevron-left"></i> Volver</a>
						<button type="submit" class="btn btn-warning btn-md"><i class="fa fa-edit"></i>&nbsp; Actualizar</button>
					</div>
				</div>
			
			</form>

            </div>
        </div>
	</div>
@endsection
