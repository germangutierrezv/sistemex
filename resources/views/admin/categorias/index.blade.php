@extends('layouts.admin')

@section('title', 'Categorias')
@section('value_nav_cat', 'active')
	

@section('content')
	<div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h2 class="panel-title"><i class="fa fa-database fa-fw"></i> Lista de Categorias</h2>
            </div>
            <div class="panel-body">
                @if (Sentry::check() && Sentry::getUser()->hasAccess('admin'))
                    <div class="text-right">
                        <a class="btn btn-success" href="{{ url('categorias/create')}}"><span class="fa fa-plus"></span> Nueva Categoria</a>
                    </div>
                @endif
                <br>
                
                @include('notifications.status_message')
                @include('notifications.errors_message')
            	
                @if($categorias->count())
                <div class="table-responsive">
                    <table class="table table-bordered table-hover table-striped">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nombre</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                        	@foreach($categorias as $categoria)
	                            <tr>
	                                <td>
	                                	<a href="{{ url('categorias',$categoria->id) }}">
	                                		{{ $categoria->id }}
	                                	</a>
	                                </td>
	                                <td>{{ $categoria->nombre }}</td>
	                                <td>
                                        @if (Sentry::check() && Sentry::getUser()->hasAccess('admin'))
                                        	<div class="btn-group">
                                                <a class="btn btn-sm btn-primary" href="{{ url('categorias',$categoria->id) }}" title="Ver"><span class="fa fa-search"></span></a>
                                                <a class="btn btn-sm btn-warning" href="{{ route('categorias.edit',$categoria->id) }}" title="Editar"><span class="fa fa-edit"></span></a>
                                                <form class="pull-right" action="{{ url('categorias', $categoria->id) }}" method="POST" style="" onsubmit="if(confirm('Estas seguro de Eliminar la Categoria?')){ return true} else {return false};">
                                                    <input type="hidden" name="_method" value="DELETE">
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                    <button type="submit" class="btn btn-sm btn-danger" title="Eliminar Categoria"><i class="fa fa-trash-o"></i></button>
                                                
                                                </form>
                                            </div>
                                        @endif
                                    </td>
	                            </tr>
	                        @endforeach
                        </tbody>
                    </table>
                    <div class="text-center">
						{{ $categorias->links() }}
					</div>
                </div>
                @else
                    <h1 class="text-primary">No hay categoría</h1>
                @endif
                <div class="text-right">
                    <a href="{{ url('/admin/blank') }}">View All Transactions <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
        </div>
    </div>

@endsection