@extends('layouts.admin')

@section('title','Detalle de Categoria')
@section('value_nav_cat', 'active')

@section('content')
<div class="col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h2 class="panel-title"><i class="fa fa-list fa-fw"></i> Detalle de la Categoria | {{ $categoria->id}} | {{ $categoria->nombre }}</h2>
            </div>
            <div class="panel-body">
            	<div class="row">
	            	<div class="col-lg-8 col-md-offset-2">
		            	<div class="table-responsive">
		                    <table class="table table-bordered table-hover table-striped">
		                        <thead>
		                        </thead>
		                        <tbody>
		                                <tr>
			                                <td><strong>ID</strong></td>
			                                <td>{{ $categoria->id }}</td>
			                            <tr>
			                            <tr>
			                                <td><strong>Nombre</strong></td>
			                                <td>{{ $categoria->nombre }}</td>
			                            <tr>
			                            <tr>
			                                <td><strong>Descripción</strong></td>
			                                <td class="text-justify">
			                                	{{ $categoria->descripcion }}
			                                </td>
			                            <tr>
			                            <tr>
			                                <td><strong>Creado</strong></td>
			                                <td>{{ getFormatoFecha($categoria->created_at) }}</td>
			                            <tr>
			                            <tr>
			                                <td><strong>Modificado</strong></td>
			                                <td>{{ getFormatoFecha($categoria->updated_at) }}</td>
			                            <tr>		                        
			                    </tbody>
		                    </table>
		                </div>
	                </div>
	                
                </div>
                <div class="pull-left">
                    <a class="btn btn-primary" href="{{ url()->previous() }}">Volver <i class="fa fa-arrow-circle-left"></i></a>
                </div>
            </div>
        </div>
    </div>
@endsection