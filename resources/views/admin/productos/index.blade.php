@extends('layouts.admin')

@section('title', 'Productos')

@section('name','csrf-token' )

@section('description','{{csrf_token()}}')

@section('css')
    <link href="{{ asset('assets/css/datatables.min.css') }}" rel="stylesheet">
@endsection

@section('value_nav_prod', 'active')
	

@section('content')
	<div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h2 class="panel-title"><i class="fa fa-database fa-fw"></i> Lista de Productos</h2>
            </div>
            <div class="panel-body">
                @if (Sentry::check() && Sentry::getUser()->hasAccess('admin'))
                    <div class="text-right">
                        <a class="btn btn-success" href="{{ url('productos/create')}}"><span class="fa fa-plus"></span> Nuevo Producto</a>
                    </div>
                @endif
                <br>
                @include('notifications.status_message')
                @include('notifications.errors_message')

                <table id="productos" class="table table-hover table-condensed">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nombre</th>
                        <th>Precio</th>
                        <th>Estatus</th>
                        <th>Categoria</th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                    </thead>
                </table>


             </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{!! asset('assets/js/datatables.min.js') !!}"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            var table = $('#productos').DataTable({
                "processing": true,
                "serverSide": true,
                "deferRender": true,
                "ajax": "{{ route('productos.prod') }}",
                "columns": [
                    { data: 'id', name: 'id',
                        fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
                            $(nTd).html("<a href='productos/"+oData.id+"'>"+oData.id+"</a>");
                        }
                    },
                    {   data: 'nombre'},
                    {   data: 'precio'},
                    {   data: 'estatus'},
                    {   data: 'categoria'},
                    {   data: null,
                        fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
                            $(nTd).html("<a class='btn btn-success btn-xs' title='Activar/Desactivar producto' href='#'><span class='fa fa-check'></span></a>");
                        },
                        width: '5px',
                        className:'activa'
                    },
                    {   data: null,
                        fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
                            $(nTd).html("<a class='btn btn-primary btn-xs' title='Ver detalle' href='productos/"+oData.id+"'><span class='fa fa-search'></span></a>");
                        },
                        width: '5px'
                    },
                    {   data: null,
                        fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
                            $(nTd).html("<a class='btn btn-warning btn-xs' title='Editar producto' href='productos/"+oData.id+"/edit'><span class='fa fa-edit'></span></a>");
                        },
                        width: '5px'
                    },
                    {   data: null,
                        fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
                            $(nTd).html("<a class='btn btn-danger btn-xs' title='Eliminar producto' href='#'><span class='fa fa-trash-o'></span></a>");
                        },
                        width: '5px',
                        className:'delete'
                    },
                    
                ],
                
                "order": [[0,'asc']]
            });
            $('#productos').on('click', 'td.delete', function () {
                var data = table.row( this ).data();
                var id = data['id'];
                var confirma=confirm("Está seguro de Eliminar el producto?");
                if(confirma){
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                        }
                    })
                    $.ajax({
                            url : 'productos/'+id,                
                            type : 'DELETE',
                            data: {
                               _token: '{!! csrf_token() !!}',
                             },
                            success : function() {
                                window.location.reload(true);
                            },
                            error: function(data) {
                                console.log('Error',data);
                                window.location.reload(true)
                            }
                        
                     });
                }
            } );

            $('#productos').on('click', 'td.activa', function () {
                var data = table.row( this ).data();
                var id = data['id'];
                console.log(data);

                var confirma=confirm("Está seguro de cambiar el estatus del producto?");
                if(confirma){
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                        }
                    })
                    console.log(id);
                    $.ajax({
                            url : 'productos/activa/'+id,                
                            type : 'PUT',
                            data: {
                               _token: '{!! csrf_token() !!}',
                             },
                            success : function(data) {
                                window.location.reload(true)
                            },
                            error: function(data) {
                                window.location.reload(true)
                                console.log('Error',data);
                            }
                        
                     });
                }
            } );
        });

        function confirmaActiva(){
            var confirma=confirm("Está seguro de cambiar el estatus del producto?");
        }    
    </script>

@endsection