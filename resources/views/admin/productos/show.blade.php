@extends('layouts.admin')

@section('title','Detalle de Producto')
@section('value_nav_prod', 'active')

@section('content')
<div class="col-lg-12">
        <div class="panel panel-{{($producto->estatus == true)?'primary':'danger'}}">
            <div class="panel-heading">
                <h2 class="panel-title"><i class="fa fa-database fa-fw"></i> Detalle del Producto | {{ $producto->id}} | {{ $producto->nombre }}</h2>
            </div>
            <div class="panel-body">
            	<div class="row">
	            	<div class="col-lg-8">
		            	<div class="table-responsive">
		                    <table class="table table-bordered table-hover table-striped">
		                        <thead>
		                        </thead>
		                        <tbody>
		                                <tr>
			                                <td><strong>ID</strong></td>
			                                <td>{{ $producto->id }}</td>
			                            <tr>
			                            <tr>
			                                <td><strong>Nombre</strong></td>
			                                <td>{{ $producto->nombre }}</td>
			                            <tr>
			                            <tr>
			                                <td><strong>Descripción</strong></td>
			                                <td class="text-justify">
			                                	{{ $producto->descripcion }}
			                                </td>
			                            <tr>
			                            <tr>
			                                <td><strong>Precio</strong></td>
			                                <td>{{ number_format($producto->precio) }}</td>
			                            <tr>
			                            <tr>
			                                <td><strong>Categoría</strong></td>
			                                <td>{{ $producto->categoria->nombre }}</td>
			                            <tr>
			                            <tr>
			                                <td><strong>Estatus</strong></td>
			                                <td>{{ ($producto->estatus == 1)? 'Activo':'Inactivo' }}</td>
			                            <tr>
			                            <tr>
			                                <td><strong>Imagen</strong></td>
			                                <td>{{ $producto->imagen }}</td>
			                            <tr>
			                            <tr>
			                                <td><strong>Creado</strong></td>
			                                <td>{{ getFormatoFecha($producto->created_at) }}</td>
			                            <tr>
			                            <tr>
			                                <td><strong>Modificado</strong></td>
			                                <td>{{ getFormatoFecha($producto->updated_at) }}</td>
			                            <tr>		                        
			                    </tbody>
		                    </table>
		                </div>
	                </div>
	                <div class="col-lg-4">
	                	<div class="form-group col-md-offset-1">
					   		<img src="{{ getImagen($producto->id) }}" width="300px" class="img">
                    	</div>
	                </div>
	                
                </div>
                <div class="pull-left">
                    <a class="btn btn-primary" href="{{ url()->previous() }}">Volver <i class="fa fa-arrow-circle-left"></i></a>
                </div>
            </div>
        </div>
    </div>
@endsection