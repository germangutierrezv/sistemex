@extends('layouts.admin')

@section('title', 'Crear Producto')
@section('value_nav_prod', 'active')

@section('content')
	<div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h2 class="panel-title"><i class="fa fa-plus fa-fw"></i> Nuevo Producto</h2>
            </div>
            <div class="panel-body">

            @include('notifications.status_message')
            @include('notifications.errors_message')

            <form action="{{ url('productos') }}" method="POST" class="form-horizontal" enctype="multipart/form-data">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">

				<div class="form-group{{ $errors->has('cedula') ? ' has-error' : '' }} has-feedback">
					<label for="cedula" class="col-md-3 control-label">
						Nombre
					</label>	
						<div class="col-md-6">
							<input type="text" class="form-control" name="nombre" id="nombre" value="{{ old('nombre') }}" required>
						</div>
				</div>
				<div class="form-group">
					<label for="descripcion" class="col-md-3 control-label">
						Descripción
					</label>	
						<div class="col-md-6">
							<textarea class="form-control" name="descripcion" id="descripcion" rows="2">{{ old('descripcion') }}
							</textarea>
						</div>
				</div>
				<div class="form-group">
					<label for="precio" class="col-md-3 control-label">
						Precio
					</label>	
						<div class="col-md-3">
							<input type="number" min="0" step="0.01" class="form-control" name="precio" id="precio" value="{{ old('precio') }}" required>
						</div>
				</div>

				<div class="form-group">
					<label for="categoria" class="col-md-3 control-label">
						Categoría
					</label>	
						<div class="col-md-3">
							<select name="idcategoria" id="idcategoria" required class="form-control">
								<option></option>
							@foreach($categorias as $key => $categoria)
								<option value="{{$key}}">{{$categoria}}</option>
							@endforeach
							</select>
						</div>
				</div>
				<div class="form-group">
					<label for="imagen" class="col-md-3 control-label">
						Imagen
					</label>	
						<div class="col-md-6">
							<input type="file" accept="image/png, .jpeg, .jpg, image/gif" class="form-control" name="imagen" id="imagen" value="{{ old('imagen') }}">
						</div>
				</div>
				<div class="form-group">
					<label for="estatus" class="col-md-3 control-label">
						Estatus
					</label>	
						<div class="col-md-3">
                            <label class="radio-inline">
                                <input type="radio" name="estatus" id="estatus" checked="checked" value="1" required> Activo   
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="estatus" id="estatus" value="0"> Inactivo
                            </label>    
                        </div>
				</div>
				<div class="form-group">
					<div class="col-md-3 col-md-offset-4">
						<a href="{{ url()->previous() }}" class="btn btn-primary"><i class="fa fa-chevron-left"></i> Volver</a>
						<button type="submit" class="btn btn-success btn-md"><i class="fa fa-plus"></i>&nbsp; Agregar</button>
					</div>
				</div>
			
			</form>

            </div>
        </div>
	</div>
@endsection
