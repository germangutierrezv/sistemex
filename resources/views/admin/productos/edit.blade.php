@extends('layouts.admin')

@section('title', 'Editar Producto')
@section('value_nav_prod', 'active')

@section('content')
	<div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h2 class="panel-title"><i class="fa fa-edit fa-fw"></i> Editar Producto #{{ $producto->id }}</h2>
            </div>
            <div class="panel-body">

            @include('notifications.status_message')
            @include('notifications.errors_message')

            <form action="{{ url('productos',$producto->id) }}" method="POST" class="form-horizontal" enctype="multipart/form-data">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" name="_method" value="PUT">


				<div class="form-group{{ $errors->has('cedula') ? ' has-error' : '' }} has-feedback">
					<label for="cedula" class="col-md-3 control-label">
						Nombre
					</label>	
						<div class="col-md-6">
							<input type="text" class="form-control" name="nombre" id="nombre" value="{{ $producto->nombre }}" required>
						</div>
				</div>
				<div class="form-group">
					<label for="descripcion" class="col-md-3 control-label">
						Descripción
					</label>	
						<div class="col-md-6">
							<textarea class="form-control" name="descripcion" id="descripcion" rows="2">{{ $producto->descripcion }}
							</textarea>
						</div>
				</div>
				<div class="form-group">
					<label for="precio" class="col-md-3 control-label">
						Precio
					</label>	
						<div class="col-md-3">
							<input type="number" min="0" step="0.01" class="form-control" name="precio" id="precio" value="{{ $producto->precio }}" required>
						</div>
				</div>

				<div class="form-group">
					<label for="categoria" class="col-md-3 control-label">
						Categoría
					</label>	
						<div class="col-md-3">
							<select name="idcategoria" id="idcategoria" required class="form-control">
							@foreach($categorias as $key => $categoria)
								<option value="{{$key}}" {{($producto->idcategoria == $key? 'selected':'' )}}>{{$categoria}}</option>
							@endforeach
							</select>
						</div>
				</div>
				<div class="form-group">
					<label for="imagen" class="col-md-3 control-label">
						Imagen
					</label>	
						<div class="col-md-3">
							<input type="file" accept="image/png, .jpeg, .jpg, image/gif" class="form-control" name="imagen" id="imagen" value="{{ $producto->imagen }}" >
						</div>
						<div class="col-md-3">
							<input type="text" class="form-control" name="imagenAnt" id="imagenAnt" value="{{ $producto->imagen }}" >
						</div>
				</div>
				<div class="form-group">
					<label for="estatus" class="col-md-3 control-label">
						Estatus
					</label>	
						<div class="col-md-3">
                            <label class="radio-inline">
                                <input type="radio" name="estatus" id="estatus" 
                     				@if ($producto->estatus)
										checked="checked"
									@endif
									value="1" required> Activo   
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="estatus" id="estatus" 
                                	@if (!$producto->estatus)
										checked="checked"
									@endif
									value="0"> Inactivo
                            </label>    
                        </div>
				</div>
				<div class="form-group">
					<div class="col-md-3 col-md-offset-4">
						<a href="{{ url()->previous() }}" class="btn btn-primary"><i class="fa fa-chevron-left"></i> Volver</a>
						<button type="submit" class="btn btn-warning btn-md"><i class="fa fa-update"></i>&nbsp; Actualizar</button>
					</div>
				</div>
			
			</form>

            </div>
        </div>
	</div>
@endsection
