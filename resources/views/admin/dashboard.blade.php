@extends('layouts.admin')

@section('title', 'Dashboard')
@section('value_nav_das', 'active')

@section('content')
			<div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
							Panel     
                        </h1>

						@include('notifications.status_message')
						@include('notifications.errors_message')
                    </div>
                </div>
                <!-- /.row -->

                
                <div class="row">
                    <div class="col-lg-4 col-md-4">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-database fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge">{{$productos}}</div>
                                        <div>Productos</div>
                                    </div>
                                </div>
                            </div>
                            <a href="{{ url('/productos') }}">
                                <div class="panel-footer">
                                    <span class="pull-left">Ver Detalles</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4">
                        <div class="panel panel-green">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-list fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge">{{ $categorias }}</div>
                                        <div>Categorías</div>
                                    </div>
                                </div>
                            </div>
                            <a href="{{ url('/categorias') }}">
                                <div class="panel-footer">
                                    <span class="pull-left">Ver Detalles</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4">
                        <div class="panel panel-yellow">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-users fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge">{{ $usuarios }}</div>
                                        <div>Usuarios!</div>
                                    </div>
                                </div>
                            </div>
                            <a href="{{ url('/users') }}">
                                <div class="panel-footer">
                                    <span class="pull-left">Ver Detalles</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    
                </div>
                <!-- /.row -->

                    <div class="col-lg-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-clock-o fa-fw"></i> Detalle</h3>
                            </div>
                            <div class="panel-body">
                                <div class="list-group">
                                    <a href="{{ url('productos') }}" class="list-group-item">
                                        <span class="badge">{{ $productosActivos }}</span>
                                        <i class="fa fa-fw fa-check"></i> Productos Activos
                                    </a>
                                    <a href="{{ url('productos') }}" class="list-group-item">
                                        <span class="badge">{{ $productos - $productosActivos }}</span>
                                        <i class="fa fa-fw fa-times"></i> Productos Inactivos
                                    </a>
                                    <a href="{{ url('categorias') }}" class="list-group-item">
                                        <span class="badge">{{ $categorias }}</span>
                                        <i class="fa fa-fw fa-list"></i> Categorias
                                    </a>
                                    <a href="{{ url('users') }}" class="list-group-item">
                                        <span class="badge">{{ $usuariosActivos }}</span>
                                        <i class="fa fa-fw fa-users"></i> Usuarios Activos
                                    </a>
                                    <a href="{{ url('users') }}" class="list-group-item">
                                        <span class="badge">{{ $usuarios - $usuariosActivos }}</span>
                                        <i class="fa fa-fw fa-user"></i> Usuarios Inactivos
                                    </a>
                                </div>
                            
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-plus fa-fw"></i> Últimos Productos Registrados</h3>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover table-striped">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Nombre</th>
                                                <th>Precio</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($ultimosproductos as $ultimo)
                                            <tr>
                                                <td>{{ $ultimo->id }}</td>
                                                <td>{{ $ultimo->nombre }}</td>
                                                <td>{{ $ultimo->precio }}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div class="text-right">
                                    <a href="{{ url('productos') }}">Ver todos <i class="fa fa-arrow-circle-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->
@endsection