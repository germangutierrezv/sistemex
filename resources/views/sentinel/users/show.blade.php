@extends('layouts.admin')

@section('title', 'Usuarios')

@section('value_nav_user', 'active')


{{-- Content --}}
@section('content')
	<?php
	    // Determine the edit profile route
	    if (($user->email == Sentry::getUser()->email)) {
	        $editAction = route('sentinel.profile.edit');
	    } else {
	        $editAction =  action('\\Sentinel\Controllers\UserController@edit', [$user->hash]);
	    }
	?>
	<div class="col-lg-12">
	    <div class="panel panel-default">
	        <div class="panel-heading">
	            <h2 class="panel-title"><i class="fa fa-users fa-fw"></i> Perfil de la cuenta</h2>
	        </div>
	        <div class="panel-body">
		        <div class="row">
			        <div class="col-lg-6">
					    @if ($user->first_name)
					    	<p><strong>First Name:</strong> {{ $user->first_name }} </p>
						@endif
						@if ($user->last_name)
					    	<p><strong>Last Name:</strong> {{ $user->last_name }} </p>
						@endif
					    <p><strong>Email:</strong> {{ $user->email }}</p>
					    
						<p>
							<em>
								Account created: {{ $user->created_at }} <br />
								Last Updated: {{ $user->updated_at }}
							</em>
						</p>
							<button class="btn btn-warning" onClick="location.href='{{ $editAction }}'">Editar Perfil</button>
					</div>
					<div class="col-lg-6">
						<h4>Group Memberships:</h4>
						<?php $userGroups = $user->getGroups(); ?>
					    <ul>
					    	@if (count($userGroups) >= 1)
						    	@foreach ($userGroups as $group)
									<li>{{ $group['name'] }}</li>
								@endforeach
							@else 
								<li>No Group Memberships.</li>
							@endif
					    </ul>
					</div>
				</div>

				<h4>User Object</h4>
				<div class="panel">
					<pre>{{ var_dump($user) }}</pre>
				</div>

	        </div>
	    </div>
	</div>

@stop
