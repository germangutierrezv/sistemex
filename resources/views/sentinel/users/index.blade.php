@extends('layouts.admin')

@section('title', 'Usuarios')

@section('value_nav_user', 'active')


{{-- Content --}}
@section('content')
<div class="col-lg-12">
	<div class="panel panel-default">
        <div class="panel-heading">
            <h2 class="panel-title"><i class="fa fa-users fa-fw"></i> Usuarios</h2>
        </div>
        <div class="panel-body">
        	@if (Sentry::check() && Sentry::getUser()->hasAccess('admin'))
		        <div class="text-right">
		            <button class="btn btn-success" onClick="location.href='{{ route('sentinel.users.create') }}'"><span class="fa fa-plus"></span> Crear Usuario</button> 
		         </div>
            @endif
            <br>
            <div class="row">
				<table class="table table-striped table-responsive">
					<thead>
						<th>Usuarios</th>
						<th>Estatus</th>
						<th>Opciones</th>
					</thead>
					<tbody>
						@foreach ($users as $user)
							<tr>
								<td><a href="{{ action('\\Sentinel\Controllers\UserController@show', array($user->hash)) }}">{{ $user->email }}</a></td>
								<td>{{ $user->status }} </td>
								<td>
									@if (Sentry::check() && Sentry::getUser()->hasAccess('admin'))
										<div class="pull-left">
											<button class="btn btn-warning btn-sm" type="button" onClick="location.href='{{ action('\\Sentinel\Controllers\UserController@edit', array($user->hash)) }}'">Editar</button>
											@if ($user->status != 'Suspended')
												<button class="btn btn-default btn-sm" type="button" onClick="location.href='{{ action('\\Sentinel\Controllers\UserController@suspend', array($user->hash)) }}'">Suspender</button>
											@else
												<button class="btn btn-default btn-sm" type="button" onClick="location.href='{{ action('\\Sentinel\Controllers\UserController@unsuspend', array($user->hash)) }}'">Un-Suspender</button>
											@endif
											@if ($user->status != 'Banned')
												<button class="btn btn-info btn-sm" type="button" onClick="location.href='{{ action('\\Sentinel\Controllers\UserController@ban', array($user->hash)) }}'">Banear</button>
											@else
												<button class="btn btn-info btn-sm" type="button" onClick="location.href='{{ action('\\Sentinel\Controllers\UserController@unban', array($user->hash)) }}'">Un-Banear</button>
											@endif
											<form action="{{ action('\\Sentinel\Controllers\UserController@destroy', array($user->hash)) }}" method="POST" style="" onsubmit="if(confirm('Estas seguro de Eliminar Usuario?')){ return true} else {return false};">
		                                        <input type="hidden" name="_method" value="DELETE">
		                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
		                                        <button type="submit" class="btn btn-sm btn-danger" title="Eliminar">Eliminar</button>
		                                            
		                                	</form>
										</div>
										
									@endif
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			<div class="row">
				{!! $users->render() !!}
			</div>
        </div>
    </div>

</div>

@endsection
