@extends('layouts.admin')

@section('title', 'Usuarios')

@section('value_nav_user', 'active')

{{-- Content --}}
@section('content')
<?php
    // Pull the custom fields from config
    $isProfileUpdate = ($user->email == Sentry::getUser()->email);
    $customFields = config('sentinel.additional_user_fields');

    // Determine the form post route
    if ($isProfileUpdate) {
        $profileFormAction = route('sentinel.profile.update');
        $passwordFormAction = route('sentinel.profile.password');
    } else {
        $profileFormAction =  route('sentinel.users.update', $user->hash);
        $passwordFormAction = route('sentinel.password.change', $user->hash);
    }
?>
<div class="col-lg-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2 class="panel-title"><i class="fa fa-user fa-fw"></i> </h2>
        </div>
        <div class="panel-body">
              <h2>
                    Editar
                    @if ($isProfileUpdate)
                        su
                    @else
                        {{ $user->email }}'s
                    @endif
                    Cuenta
                </h2>


            <?php $customFields = config('sentinel.additional_user_fields'); ?>

            @if (! empty($customFields))

            <div class="col-lg-4 col-md-12">
                <div class="col-md-offset-1">
                    <form method="POST" action="{{ $profileFormAction }}" accept-charset="UTF-8" class="form-horizontal" role="form">
                        <input name="_method" value="PUT" type="hidden">
                        <input name="_token" value="{{ csrf_token() }}" type="hidden">

                        <h4 class="text-primary">Perfil</h4>

                        @foreach(config('sentinel.additional_user_fields') as $field => $rules)
                        <div class="row">
                            <div class="col-md-4">
                                <label for="right-label" class="right inline">{{ ucwords(str_replace('_',' ',$field)) }}</label>
                            </div>
                            <div class="col-md-8{{ ($errors->has($field)) ? 'has-error' : '' }}">
                                <input class="form-control" name="{{ $field }}" type="text" value="{{ Request::old($field) ? Request::old($field) : $user->$field }}">
                                {{ ($errors->has($field) ? $errors->first($field, '<small class="error">:message</small>') : '') }}
                            </div>
                        </div>
                        @endforeach
                        <br>
                        <div class="col-md-1">
                            <input class="btn btn-warning" value="Guardar Cambios" type="submit">
                        </div>
                    
                    </form>
                </div>
            </div>
            @endif

            @if (Sentry::getUser()->hasAccess('admin') && ($user->hash != Sentry::getUser()->hash))
            <div class="col-lg-4 col-md-12">
                <form method="POST" action="{{ route('sentinel.users.memberships', $user->hash) }}" accept-charset="UTF-8" class="form-horizontal" role="form">
                    <div class="col-lg-4 col-md-4">
                        <div class="col-md-offset-1">
                            <h4 class="text-primary">Miembro/Grupo</h4>

                            <div class="row">
                                <div class="col-md-3 col-md-offset-1">
                                    @foreach($groups as $group)
                                        <label class="checkbox-inline">
                                            <input type="checkbox" name="groups[{{ $group->name }}]" value="1" {{ ($user->inGroup($group) ? 'checked' : '') }}> {{ $group->name }}
                                        </label>
                                    @endforeach
                                </div>
                            </div>
                            <br>
                            <div class="col-md-1">
                                <input name="_token" value="{{ csrf_token() }}" type="hidden">
                                <input class="btn btn-warning" value="Actualizar Memberships" type="submit">
                            </div>
                            
                        </div>
                    </div>
                </form>
            </div>
            @endif

            <div class="col-lg-4 col-md-12">
                
                <form method="POST" action="{{ $passwordFormAction }}" accept-charset="UTF-8" class="form-inline" role="form">
                    <div class="row">
                        <div class="small-6 large-centered columns">
                            <h4 class="text-primary">Cambiar Contraseña</h4>

                            <div class="row">
                                <div class="col-md-6">
                                    <label for="right-label" class="right inline">Contraseña Actual</label>
                                </div>
                                <div class="small-9 columns">
                                    <input class="form-control" placeholder="Old Password" name="oldPassword" value="" id="oldPassword" type="password">
                                    {{ ($errors->has('oldPassword') ?  $errors->first('oldPassword', '<small class="error">:message</small>') : '') }}
                                </div>
                            </div>

                             <div class="row">
                                <div class="col-md-6">
                                    <label for="right-label" class="right inline">Nueva Contraseña</label>
                                </div>
                                <div class="small-9 columns">
                                    <input class="form-control" placeholder="New Password" name="newPassword" value="" id="newPassword" type="password">
                                    {{ ($errors->has('newPassword') ?  $errors->first('newPassword', '<small class="error">:message</small>') : '') }}
                                </div>
                            </div>

                             <div class="row">
                                <div class="col-md-6">
                                    <label for="right-label" class="right inline">Confirmar Contraseña</label>
                                </div>
                                <div class="small-9 columns">
                                    <input class="form-control" placeholder="Confirm New Password" name="newPassword_confirmation" value="" id="newPassword_confirmation" type="password">
                                    {{ ($errors->has('newPassword_confirmation') ?  $errors->first('newPassword_confirmation', '<small class="error">:message</small>') : '') }}
                                </div>
                            </div>
                            <BR>

                                <div class="col-md-1">
                                    <input name="_token" value="{{ csrf_token() }}" type="hidden">
                                    <input class="btn btn-success" value="Cambiar Contraseña" type="submit">
                                </div>

                        </div>
                    </div>
                </form>

            </div>




        </div>
    </div>
</div>

@stop