@extends('layouts.admin')

@section('title', 'Crear Usuario')

@section('value_nav_user', 'active')


{{-- Content --}}
@section('content')
<div class="col-lg-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2 class="panel-title"><i class="fa fa-users fa-fw"></i> Usuarios</h2>
        </div>
        <div class="panel-body">
            <form method="POST" action="{{ route('sentinel.users.store') }}" accept-charset="UTF-8">
                    <h2>Crear Nuevo Usuario</h2>
                    <div class="container">
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-1">
                                    <label for="right-label" class="control-label">Usuario</label>
                                </div>
                                <div class="col-md-3 {{ ($errors->has('username')) ? 'error' : '' }}">
                                    <input class="form-control" placeholder="Username" name="username" type="text" value="{{ Request::old('username') }}">
                                    {{ ($errors->has('username') ? $errors->first('username', '<small class="error">:message</small>') : '') }}
                                </div>
                            </div>
                        </div>    
                        <br>
                         <div class="row">   
                            <div class="form-group">
                                <div class="col-md-1">
                                    <label for="right-label" class="control-label">E-mail</label>
                                </div>
                                <div class="col-md-3 {{ ($errors->has('email')) ? 'error' : '' }}">
                                    <input class="form-control" placeholder="E-mail" name="email" type="text" value="{{ Request::old('email') }}">
                                    {{ ($errors->has('email') ? $errors->first('email', '<small class="error">:message</small>') : '') }}
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-1">
                                    <label for="right-label" class="right inline">Contraseña</label>
                                </div>
                                <div class="col-md-3">
                                    <input class="form-control" placeholder="Password" name="password" value="" type="password">
                                    {{ ($errors->has('password') ?  $errors->first('password', '<small class="error">:message</small>') : '') }}
                                </div>
                            </div>
                            
                        </div>
                        <br>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-1">
                                    <label for="right-label" class="right inline">Confirmar Contraseña</label>
                                </div>
                                <div class="col-md-3">
                                    <input class="form-control" placeholder="Confirm Password" name="password_confirmation" value="" type="password">
                                    {{ ($errors->has('password_confirmation') ?  $errors->first('password_confirmation', '<small class="error">:message</small>') : '') }}
                                </div>
                            </div>
                        </div>
                        <div class="row">    
                            <div class="checkbox">
                                <div class="col-md-offset-2">
                                    <input class="control-label" name="activate" value="activate" type="checkbox">
                                    <label for="activate">Activo</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                             <div class="form-group">
                                <div class="col-md-3">
                                    <input name="_token" value="{{ csrf_token() }}" type="hidden">
                                    <input class="btn btn-info" value="Crear" type="submit">
                                </div>
                            </div>
                        </div>
                        
                    </div>
            </form>
        </div>
    </div>
</div>
@stop