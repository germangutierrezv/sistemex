<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="robots" content="noindex">

    <title>Iniciar Sesión</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link rel="stylesheet" id="auth-css" href="{{ asset('assets/css/auth.css') }}" type="text/css" media="all">
    <!--<script src="http://code.jquery.com/jquery-1.10.2.min.js"></script>-->
    <script src="{!! asset('assets/js/jquery.min.js') !!}"></script>

    <!--<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>-->
    <link rel="stylesheet" id="auth-css" href="{{ asset('assets/css/bootstrap.min.css') }}" type="text/css" media="all">
</head>
<body>
    <div class="container">
        <div class="card card-container">
            <div class="col-md-offset-3">
                <h1>Iniciar Sesión</h1>    
            </div>
            
            <img id="profile-img" class="profile-img-card" src="http://ssl.gstatic.com/accounts/ui/avatar_2x.png" />
                <p id="profile-name" class="profile-name-card"></p>
                <form method="POST" action="{{ route('sentinel.session.store') }}" accept-charset="UTF-8">
                    <div class="row">
                        <div class="col-md-4">
                            <label for="right-label">Email</label>
                        </div>
                        <div class="col-md-8 {{ ($errors->has('email')) ? 'error' : '' }}">
                            <input class="form-control" placeholder="Email" autofocus="autofocus" name="email" type="text"  value="{{ Request::old('email') }}">
                            {{ ($errors->has('email') ? $errors->first('email', '<small class="error">:message</small>') : '') }}
                        </div>
                    </div>
                    <br>

                    <div class="row">
                        <div class="col-md-4">
                            <label for="right-label">Contraseña</label>
                        </div>
                        <div class="col-md-8">
                            <input class="form-control" placeholder="Password" name="password" value="" type="password">
                            {{ ($errors->has('password') ?  $errors->first('password', '<small class="error">:message</small>') : '') }}
                        </div>
                    </div>
                    <br>

                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <input name="_token" value="{{ csrf_token() }}" type="hidden">
                            <input class="btn btn-lg btn-primary btn-block btn-signin" value="Ingresar" type="submit">
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <a class="button secondary" href="{{ route('sentinel.forgot.form') }}">Olvido Contraseña</a>
                        </div>
                    </div>
                    
                </form>
            </div>
        </div>
    </body>
</html>