@extends('layouts.admin')

@section('title', 'Nuevo Grupo')
@section('value_nav_grupo', 'active')

{{-- Content --}}
@section('content')

<div class="col-lg-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2 class="panel-title"><i class="fa fa-group fa-fw"></i> </h2>
        </div>
        <div class="panel-body">
            <form method="POST" action="{{ route('sentinel.groups.store') }}" accept-charset="UTF-8">
                <div class="row">
                    <div class="col-md-12">
                        <h2>Nuevo Grupo</h2>
                    </div>
                 </div>
                    <div class="row">
                        <div class="col-md-1">
                            <label for="right-label" class="right inline">Nombre</label>
                        </div>
                        <div class="col-md-3 {{ ($errors->has('name')) ? 'error' : '' }}">
                            <input class="form-control" placeholder="Name" name="name" type="text">
                            {{ ($errors->has('name') ? $errors->first('name', '<small class="error">:message</small>') : '') }}
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-1">
                            <label for="Permissions">Permisos</label>                        
                        </div>
                        <?php $defaultPermissions = config('sentinel.default_permissions', []); ?>
                        @foreach ($defaultPermissions as $permission)
                            <div class="col-md-1">
                                <input name="permissions[{{ $permission }}]" value="1" type="checkbox"
                                @if (Request::old('permissions[' . $permission .']'))
                                    checked
                                @endif
                                > {{ ucwords($permission) }}
                            </div>
                        @endforeach
                    </div>

                    <div class="row">
                        <div class="col-md-1">
                            <input name="_token" value="{{ csrf_token() }}" type="hidden">
                            <input class="btn btn-primary" value="Crear Grupo" type="submit">
                        </div>
                    </div>


            </form>
        </div>
    </div>
</div>



@stop