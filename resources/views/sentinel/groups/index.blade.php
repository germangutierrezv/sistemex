@extends('layouts.admin')

@section('title', 'Grupos')
@section('value_nav_grupo', 'active')

{{-- Content --}}
@section('content')

<div class="col-lg-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2 class="panel-title"><i class="fa fa-group fa-fw"></i> Lista de Grupos</h2>
        </div>
        <div class="panel-body">
			<div class="row">
				<div class="col-md-4">
					<h1>Grupos Disponibles</h1>
				</div>
				@if (Sentry::check() && Sentry::getUser()->hasAccess('admin'))
					<div class="text-right col-md-8">
						<button class="btn btn-success" onClick="location.href='{{ route('sentinel.groups.create') }}'">
							<span class="fa fa-plus"></span>Nuevo Grupo</button>
					</div>
				@endif
			</div>
			<div class="row">
				<table class="table table-striped table-responsive">
					<thead>
						<th>Nombre</th>
						<th>Permisos</th>
						<th></th>
					</thead>
					<tbody>
					@foreach ($groups as $group)
						<tr>
							<td>
								<a href="groups/{{ $group->hash }}">{{ $group->name }}</a>
							</td>
							<td>
								<?php
									$permissions = $group->getPermissions();
									$keys = array_keys($permissions);
									$last_key = end($keys);
								?>
								@foreach ($permissions as $key => $value)
									{{ ucfirst($key) . ($key == $last_key ? '' : ', ') }}
								@endforeach
							</td>
							<td>
								@if (Sentry::check() && Sentry::getUser()->hasAccess('admin'))
									<button class="btn btn-warning" onClick="location.href='{{ action('\\Sentinel\Controllers\GroupController@edit', array($group->hash)) }}'">Editar</button>
	                            @endif
							 </td>
						</tr>
					@endforeach
					</tbody>
				</table>
			</div>
			<div class="row">
				{!! $groups->render() !!}
			</div>
        </div>
    </div>
</div>


<!--
	The delete button uses Resftulizer.js to restfully submit with "Delete".  The "action_confirm" class triggers an optional confirm dialog.
	Also, I have hardcoded adding the "disabled" class to the Admin group - deleting your own admin access causes problems.
-->
@stop

