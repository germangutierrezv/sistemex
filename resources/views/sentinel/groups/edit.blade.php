@extends('layouts.admin')

@section('title', 'Editar Grupo')
@section('value_nav_grupo', 'active')


{{-- Content --}}
@section('content')

<div class="col-lg-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2 class="panel-title"><i class="fa fa-group fa-fw"></i> </h2>
        </div>
        <div class="panel-body">
            <form method="POST" action="{{ route('sentinel.groups.update', $group->hash) }}" accept-charset="UTF-8">
                <div class="row">
                    <div class="col-md-12">
                        <h2>Editar Grupos</h2>
                    </div>
                 </div>
                <div class="row">
                    <div class="col-md-1">
                        <label for="right-label" class="right inline">Nombre</label>
                    </div>
                    <div class="col-md-4 {{ ($errors->has('name')) ? 'error' : '' }}">
                        <input class="form-control" placeholder="Name" name="name" value="{{ Request::old('name') ? Request::old('name') : $group->name }}" type="text">
                        {{ ($errors->has('name') ? $errors->first('name', '<small class="error">:message</small>') : '') }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-1">
                        <label class="right inline">Permisos</label>                        
                    </div>
                    <?php $defaultPermissions = config('sentinel.default_permissions', []); ?>
                    @foreach ($defaultPermissions as $permission)
                        <div class="col-md-1">
                           <input name="permissions[{{ $permission }}]" value="1" type="checkbox" {{ (isset($permissions[$permission]) ? 'checked' : '') }}>
                           {{ ucwords($permission) }}
                        </div>
                    @endforeach
                </div>
                <div class="row">
                    <div class="col-md-1 col-md-offset-1">
                        <input name="id" value="{{ $group->hash }}" type="hidden">
                        <input name="_method" value="PUT" type="hidden">
                        <input name="_token" value="{{ csrf_token() }}" type="hidden">
                        <input class="btn btn-success" value="Guardar" type="submit">
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>

@stop
