<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Tienda Virtual de SISTEMEX">
    <meta name="author" content="German Gutierrez">

    <title>@yield('title')</title>
    
    <meta name="@yield('name')" content="@yield('description')">

    <!-- Bootstrap Core CSS -->
    <!--<link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">-->
    <link href="{{ asset('assets/css/bootstrap_3.37.min.css') }}" rel="stylesheet">
    

    <!-- Custom CSS -->
    <link href="{{ asset('assets/css/admin.css') }}" rel="stylesheet">
    
    <!-- Morris Charts CSS -->
    <link href="{{ asset('assets/css/plugins/morris.css') }}" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="{{ asset('assets/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">

    @yield('css')

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ url('/dashboard') }}">Panel de Control ({{ config('app.name')}})</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                <li class="dropdown">
                     @if (Sentry::check())
                    <li {!! (Request::is('profile') ? 'class="active"' : '') !!}>
                        <a href="{{ route('sentinel.profile.show') }}">{{ Sentry::getUser()->email }}</a>
                    </li>
                    <li>
                        <a href="{{ route('sentinel.logout') }}">Salir</a>
                    </li>
                @else
                    <li {!! (Request::is('login') ? 'class="active"' : '') !!}>
                        <a href="{{ route('sentinel.login') }}">Iniciar Sesión</a>
                    </li>
                    <li {!! (Request::is('register') ? 'class="active"' : '') !!}>
                        <a href="{{ route('sentinel.register.form') }}">Registrar</a>
                    </li>
                @endif
                    
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li class="@yield('value_nav_das')">
                        <a href="{{ url('/dashboard') }}"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                    </li>
                    @if (Sentry::check() && Sentry::getUser()->hasAccess('admin'))
                        <li class="@yield('value_nav_prod')">
                            <a href="{{ url('/productos') }}"><i class="fa fa-fw fa-database"></i> Productos</a>
                        </li>
                        <li class="@yield('value_nav_cat')">
                            <a href="{{ url('/categorias') }}"><i class="fa fa-fw fa-list-ul"></i> Categorias</a>
                        </li>
                        <li class="@yield('value_nav_user')">
                            <a href="{{ url('/users') }}"><i class="fa fa-fw fa-users"></i> Usuarios</a>
                        </li>
                        <li class="@yield('value_nav_grupo')">
                            <a href="{{ url('/groups') }}"><i class="fa fa-fw fa-users"></i> Grupos</a>
                        </li>
                        
                    @endif
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">
                @yield('content')
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <!--<script src="http://code.jquery.com/jquery-1.10.2.min.js"></script>-->
    <script src="{!! asset('assets/js/jquery-2.1.3.min.js') !!}"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>

    @yield('scripts')
    
    <!-- Morris Charts JavaScript -->
    <script src="{!! asset('assets/js/plugins/morris/raphael.min.js') !!}"></script>
    <script src="{!! asset('assets/js/plugins/morris/morris.min.js') !!}"></script>
    <script src="{!! asset('assets/js/plugins/morris/morris-data.js') !!}"></script>

    
</body>

</html>