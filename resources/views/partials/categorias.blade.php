  <div class="col-lg-3">
    <h1>Tienda Virtual</h1>
    <h4>Categorias</h4>
    <div class="list-group">
      @if($categorias->count())
        @foreach ($categorias as $categ)
          <a href="{{ url('categoria',$categ->nombre) }}" class="list-group-item item{{ $categ->id }}">{{ $categ->nombre }}</a>
        @endforeach 
      @else
        <h2>No hay categoría</h2>
      @endif
    </div>
    <br>
  </div>
