@extends('layouts.front')

@section('title', 'Tienda Virtual SISTEMEX')

@section('content')

  @include('partials.categorias')
    
    <div class="col-lg-9">

      <div id="carouselExampleIndicators" class="carousel slide my-4" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner" role="listbox">
          <div class="carousel-item active">
            <img class="d-block img-fluid" src="{{ asset('assets/img/slide/slide1.jpg') }}" width="900" height="350" alt="First slide">
          </div>
          <div class="carousel-item">
            <img class="d-block img-fluid" src="{{ asset('assets/img/slide/slide2.jpg') }}" width="900" height="350" alt="Second slide">
          </div>
          <div class="carousel-item">
            <img class="d-block img-fluid" src="{{ asset('assets/img/slide/slide3.jpg') }}" alt="Third slide">
          </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
      @if($productos->count())
      <div class="row">
      	@foreach($productos as $producto)
	       <div class="col-lg-4 col-md-6 mb-4">
          <div class="card h-100">
            <a href="#"><img class="card-img-top" src="{{ getImagen($producto->id) }}" width="300" alt=""></a>
            <div class="card-body">
              <h4 class="card-title">
                <a href="{{ url('/detalle',$producto->id) }}">{{ $producto->nombre}}</a>
              </h4>
              <h5>${{ $producto->precio}}</h5>
              <p class="card-text-small">{{ $producto->descripcion}}</p>
            </div>
            <div class="card-footer">
              <small class="text-muted">&#9733; &#9733; &#9733; &#9733; &#9734;</small>
              <div class="pull-right">
                <a href="{{ url('/detalle',$producto->id) }}">Leer más</a>  
              </div>
              
            </div>
          </div>
        </div>
      	@endforeach

      </div>
      @else
        <h1>No hay productos</h1>
      @endif
      <!-- /.row -->          
        
      <div class="pagination">
            {{ $productos->links("pagination::bootstrap-4") }}
      </div>
    </div>
    <!-- /.col-lg-9 -->

@endsection
