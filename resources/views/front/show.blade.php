@extends('layouts.front')

@section('title','Detalle del Producto')

@section('content')

	@include('partials.categorias')
  
	<div class="col-lg-9">

	  <div class="card mt-4">
	    <img class="card-img-top img-fluid" src="{{ getImagen($producto->id) }}" width="100px" height="100px" alt="{{ $producto->nombre}}">
	    <div class="card-body">
	      <h3 class="card-title">{{ $producto->nombre}}</h3>
	      <h4>Precio: ${{ number_format($producto->precio,2) }}</h4>
	      <p class="card-text text-justify">{{ $producto->descripcion }}</p>
	      
	      <div>
	      	<span class="text-primary">Categoria </span>{{ $producto->categoria->nombre }}	
	      </div>
	      
	      
	      <span class="text-warning">&#9733; &#9733; &#9733; &#9733; &#9734;</span>
	      4.0 stars
	    </div>
	  </div>
	  <!-- /.card -->
	  <div class="btn">
	  	<a href="{{ url()->previous() }}" class="btn btn-primary"><span class="fa fa-backward"></span> Volver</a>	
	  </div>
	  
@endsection