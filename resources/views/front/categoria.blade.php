@extends('layouts.front')

@section('title','Tienda Virtual SISTEMEX - Categoria')

@section('content')

@include('partials.categorias')

    <div class="col-lg-9">
    	<h1>{{ $categoria}}</h1>
      	<div class="row">
		    @if($productos->count())  	
				     @foreach($productos as $producto)
					    <div class="col-lg-4 col-md-6 mb-4">
				     	    <div class="card h-100">
				        	    <a href="#"><img class="card-img-top" src="{{ getImagen($producto->id) }}" width="700" alt=""></a>
				            	<div class="card-body">
				              		<h4 class="card-title">
				                		<a href="{{ url('/detalle',$producto->id) }}">{{ $producto->nombre}}</a>
				              		</h4>
				              		<h5>Precio: ${{ $producto->precio}}</h5>
				              		<p class="card-text-small">{{ $producto->descripcion}}</p>
				            	</div>
				            	<div class="card-footer">
				              		<small class="text-muted">&#9733; &#9733; &#9733; &#9733; &#9734;</small>
				              		<div class="pull-right">
				                	<a href="{{ url('/detalle',$producto->id) }}">Leer más</a>  
				              	</div>
				             
				            </div>
				          </div>
				        </div>
			      	@endforeach
			      	<div class="pagination">
			            {{ $productos->links("pagination::bootstrap-4") }}
			      </div>
			@else
				<h2 id="noproducto">No hay productos en esta categoria</h2>
			@endif

      	</div>
      <!-- /.row -->          
   </div>      

@endsection


@section('scripts')
<script type="text/javascript">
	$(document).ready(function(){
	   $('.list-group a').click(function(e) {
	    
	 		$(this).addClass('active').siblings().removeClass('active');
	    
	    });
	});
</script>
@endsection